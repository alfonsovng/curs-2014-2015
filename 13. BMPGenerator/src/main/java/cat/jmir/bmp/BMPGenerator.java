package cat.jmir.bmp;

import java.io.IOException;
import java.io.OutputStream;

public class BMPGenerator {

    private static final int COLUMNES = 111;
    private static final int FILES = 145;
	
	private final int r;
	private final int g;
	private final int b;
	
	public BMPGenerator(int r, int g, int b) {
		this.r = r % 256;
		this.g = g % 256;
		this.b = b % 256;
	}
	
	public void write(OutputStream output) throws IOException {
		/*
         * Cada fila ha de ser múltiple de 4 i, si no ho és,
         * s'afegeixen tans zeros com faci falta. Això
         * s'ha de tenir en compte per a posar-ho a la capçalera 
         * per que afecta a la mida total del BMP.
         */
        int extraRowBytes = (4 - (COLUMNES*3) % 4) % 4;
         
        writeHeader(output, extraRowBytes);
     
        for(int i=0;i<FILES;++i) {
            for(int j=0;j<COLUMNES;++j) {
                output.write(b); //b
                output.write(g); //g
                output.write(r); //r
            }
            for(int j=0;j<extraRowBytes;++j) {
                output.write(0); //extra per fila
            }
        }
	}
	
	/**
     * Escriu la capçaera BMP. Més informació a 
     * http://es.wikipedia.org/wiki/Windows_bitmap
     * 
     * @param output on s'escriurà la capçalera
     * @param extraRowBytes els bytes extres per fila
     * @throws IOException si es produeix cap error
     */
    private void writeHeader(OutputStream output, int extraRowBytes) throws IOException {
        int midaImatge = (COLUMNES*3 + extraRowBytes) * FILES;
         
        output.write(new byte[] { 66, 77} ); //Tipo de fichero "BM"
        output.write(toFourBytes(midaImatge+54)); //Tamaño del archivo
        output.write(new byte[] { 0, 0} ); //Reservado
        output.write(new byte[] { 0, 0} ); //Reservado
        output.write(new byte[] { 54, 0, 0, 0} ); //Inicio de los datos de la imagen
        output.write(new byte[] { 40, 0, 0, 0} ); //Tamaño de la cabecera del bitmap
        output.write(toFourBytes(COLUMNES)); //Anchura (píxels)
        output.write(toFourBytes(FILES)); //Altura (píxels)
        output.write(new byte[] { 1, 0} ); //Número de planos
        output.write(new byte[] { 24, 0} ); //Tamaño de cada punto
        output.write(new byte[] { 0, 0, 0, 0} ); //Compresión (0=no comprimido)
        output.write(toFourBytes(midaImatge)); //Tamaño de la imagen
        output.write(new byte[] { 19, 11, 0, 0} ); //Resolución horizontal
        output.write(new byte[] { 19, 11, 0, 0} ); //Resolución vertical
        output.write(new byte[] { 0, 0, 0, 0} ); //Tamaño de la tabla de color
        output.write(new byte[] { 0, 0, 0, 0} ); //Contador de colores importantes
    }
     
    /**
     * Concerteix un enter a una representació en forma d'array de bytes
     * tal i com es fa servir a la capçalera del format BMP
     * 
     * @param value enter a transformar
     * @returnun l'enter transformat en un array de 4 bytes 
     */
    private byte[] toFourBytes(int value) {
        return new byte[] {
                (byte)(value),
                (byte)(value >>> 8),
                (byte)(value >>> 16),
                (byte)(value >>> 24),
        };
    }
}
