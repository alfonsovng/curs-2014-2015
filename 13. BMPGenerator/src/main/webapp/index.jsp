<%@ page contentType="image/bmp" import="cat.jmir.bmp.BMPGenerator, java.util.Random" %><%

	int r = getColor(request, "r");
	int g = getColor(request, "g");
	int b = getColor(request, "b");

	BMPGenerator generator = new BMPGenerator(r, g, b);
	generator.write(response.getOutputStream());
%><%! 
 	
	private Random random = new Random();

	private int getColor(HttpServletRequest request, String color) { 
    	try {
    		return Integer.parseInt(request.getParameter(color));
      	} catch (Exception ignored) {
      	}
      	return random.nextInt(256);
	} 
%>