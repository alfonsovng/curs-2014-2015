package cat.jmir;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cat.jmir.WordCounter.WordCounter;

/**
 * Servlet implementation class WordCounterServletJSON
 */
public class WordCounterServletJSON extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String text = request.getParameter("text");
		String word = null;
		if(text != null && !text.trim().isEmpty()) {
			
			WordCounter wc = new WordCounter(text, true);
			word = wc.getMaxOccurrencesWord();
		} else {
			word = ""; 
		}
		response.getWriter().print("{\"maxOccurrencesWord\":\"" + word + "\"}");
	}
}
