package cat.jmir;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cat.jmir.WordCounter.WordCounter;

/**
 * Servlet implementation class WordCounterServlet
 */
public class WordCounterServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public WordCounterServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
//		String nom = request.getParameter("nom");
//		
//		response.getWriter().write("<html><body><h1>Hola, " + nom + "!</h1></body></html>" );		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String text = request.getParameter("text");
 		
		if(text == null || text.trim().isEmpty()) {
			
			request.setAttribute("missatge", "El valor de text és null");
			
			getServletConfig().getServletContext()
				.getRequestDispatcher("/error.jsp").forward(request,response);
			return;
		}
			 
		
		WordCounter wc = new WordCounter(text, true);
		
		String word = wc.getMaxOccurrencesWord();
		
		
		request.setAttribute("maxOccurrencesWord", word);

		getServletConfig().getServletContext()
			.getRequestDispatcher("/resposta.jsp").forward(request,response);
	}

	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
