<html>
<head>
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script>
$(document).ready(function(){
  $("#maxOccurrencesWordButton").click(function(){
    $.getJSON("WordCounterServletJSON", {text: $("#text").val()}, function(result){
      $("#maxOccurrencesWordDiv").html('<br /> La paraula m�s com� �s ' + result.maxOccurrencesWord);
      $("#maxOccurrencesWordDiv").append('<br /><br /><i>I aquest �s el json que he rebut: ' + JSON.stringify(result));
    });
  });
});
</script>
</head>
<body>
<h1>Versi� normal</h1>
<form method="post" action="WordCounterServlet">
<textarea id="text" name="text" rows="10" cols="30">
</textarea>
<br />
<input type="submit" value="Diga'm quina �s la paraula que surt m�s"/>
</form>
<hr />
<h1>Versi� AJAX + JSON</h1>
<button id="maxOccurrencesWordButton" value="go">Diga'm quina �s la paraula que surt m�s amb AJAX</button>
<div id="maxOccurrencesWordDiv"></div>
</body>
</html>
