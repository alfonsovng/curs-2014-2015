var classexercici5_1_1figures_1_1f2d_1_1Rectangle =
[
    [ "Rectangle", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#a5fb92be2dc052ee8dd1b51281f7e0afd", null ],
    [ "calculaArea", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#a85e10ae4af5e4343d8492ff582cee6fe", null ],
    [ "calculaPerimetre", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#a05480f704c9e7cb9c165e97fe11d30f3", null ],
    [ "getAltura", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#ad12d60eb1e78dccc0a69f6380e5955cf", null ],
    [ "getBase", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#a950e75f6278c4ad020121de739b1e97f", null ],
    [ "setAltura", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#aca434d276b638e34dab727654abc6a73", null ],
    [ "setBase", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#aab1663358b0b6fa928253172b850cc50", null ],
    [ "toString", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html#a3263faf656e35f655159ed189d12b7b1", null ]
];