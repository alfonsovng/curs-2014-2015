var annotated =
[
    [ "exercici5", null, [
      [ "figures", null, [
        [ "f2d", null, [
          [ "Cercle", "classexercici5_1_1figures_1_1f2d_1_1Cercle.html", "classexercici5_1_1figures_1_1f2d_1_1Cercle" ],
          [ "Figura2D", "classexercici5_1_1figures_1_1f2d_1_1Figura2D.html", "classexercici5_1_1figures_1_1f2d_1_1Figura2D" ],
          [ "Hexagon", "classexercici5_1_1figures_1_1f2d_1_1Hexagon.html", "classexercici5_1_1figures_1_1f2d_1_1Hexagon" ],
          [ "Quadrat", "classexercici5_1_1figures_1_1f2d_1_1Quadrat.html", "classexercici5_1_1figures_1_1f2d_1_1Quadrat" ],
          [ "Rectangle", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html", "classexercici5_1_1figures_1_1f2d_1_1Rectangle" ]
        ] ],
        [ "f3d", null, [
          [ "Cub", "classexercici5_1_1figures_1_1f3d_1_1Cub.html", "classexercici5_1_1figures_1_1f3d_1_1Cub" ],
          [ "Esfera", "classexercici5_1_1figures_1_1f3d_1_1Esfera.html", "classexercici5_1_1figures_1_1f3d_1_1Esfera" ],
          [ "Figura3D", "classexercici5_1_1figures_1_1f3d_1_1Figura3D.html", "classexercici5_1_1figures_1_1f3d_1_1Figura3D" ]
        ] ],
        [ "FiguraGeometrica", "classexercici5_1_1figures_1_1FiguraGeometrica.html", "classexercici5_1_1figures_1_1FiguraGeometrica" ]
      ] ],
      [ "FiguresGeometriquesTest", "classexercici5_1_1FiguresGeometriquesTest.html", null ]
    ] ]
];