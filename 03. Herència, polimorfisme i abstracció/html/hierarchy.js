var hierarchy =
[
    [ "exercici5.figures.FiguraGeometrica.Color", "enumexercici5_1_1figures_1_1FiguraGeometrica_1_1Color.html", null ],
    [ "exercici5.figures.FiguraGeometrica", "classexercici5_1_1figures_1_1FiguraGeometrica.html", [
      [ "exercici5.figures.f2d.Figura2D", "classexercici5_1_1figures_1_1f2d_1_1Figura2D.html", [
        [ "exercici5.figures.f2d.Cercle", "classexercici5_1_1figures_1_1f2d_1_1Cercle.html", null ],
        [ "exercici5.figures.f2d.Hexagon", "classexercici5_1_1figures_1_1f2d_1_1Hexagon.html", null ],
        [ "exercici5.figures.f2d.Rectangle", "classexercici5_1_1figures_1_1f2d_1_1Rectangle.html", [
          [ "exercici5.figures.f2d.Quadrat", "classexercici5_1_1figures_1_1f2d_1_1Quadrat.html", null ]
        ] ]
      ] ],
      [ "exercici5.figures.f3d.Figura3D", "classexercici5_1_1figures_1_1f3d_1_1Figura3D.html", [
        [ "exercici5.figures.f3d.Cub", "classexercici5_1_1figures_1_1f3d_1_1Cub.html", null ],
        [ "exercici5.figures.f3d.Esfera", "classexercici5_1_1figures_1_1f3d_1_1Esfera.html", null ]
      ] ]
    ] ],
    [ "exercici5.FiguresGeometriquesTest", "classexercici5_1_1FiguresGeometriquesTest.html", null ]
];