package exercici5;

import java.util.ArrayList;

import exercici5.figures.FiguraGeometrica;
import exercici5.figures.FiguraGeometrica.Color;
import exercici5.figures.f2d.Cercle;
import exercici5.figures.f2d.Hexagon;
import exercici5.figures.f2d.Quadrat;
import exercici5.figures.f2d.Rectangle;
import exercici5.figures.f3d.Cub;
import exercici5.figures.f3d.Esfera;

public class FiguresGeometriquesTest {

	public static void main(String[] args) {
		
		ArrayList<FiguraGeometrica> llista = llegeixFigures();
		
		double areaTotal = 0;
		for(int i=0;i<llista.size();++i) {
			FiguraGeometrica f = llista.get(i);
			areaTotal+=f.calculaArea();
		}
		
		System.out.println("L'àrea total és " + areaTotal);
	}

	private static ArrayList<FiguraGeometrica> llegeixFigures() {
		Cercle c = new Cercle(22);
		Hexagon h = new Hexagon(33);
		Rectangle r = new Rectangle(20, 30);
		Quadrat q = new Quadrat(15);
		Esfera e = new Esfera(9.5); //radi de l'esfera
		Cub u = new Cub(33); //aresta del cub
		
		c.setColor(Color.GROC);
		
		
		ArrayList<FiguraGeometrica> llista = new ArrayList<FiguraGeometrica>();
		llista.add(c);
		llista.add(h);
		llista.add(r);
		llista.add(q);
		llista.add(e);
		llista.add(u);
		
		return llista;
	}
	
	
	
	
	
	
	
	
	
	
}
