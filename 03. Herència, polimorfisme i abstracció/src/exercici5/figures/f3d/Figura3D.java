package exercici5.figures.f3d;

import exercici5.figures.FiguraGeometrica;

public abstract class Figura3D extends FiguraGeometrica {
	
	public abstract double calculaVolum();
}
