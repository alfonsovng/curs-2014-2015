package exercici5.figures.f3d;

public class Cub extends Figura3D {

	private double aresta;
	
	public Cub(double aresta) {
		setAresta(aresta);
	}
	
	public double calculaArea() {
		
		return Math.pow(aresta, 2) * 6;
	}
	
	public double calculaVolum() {
		return Math.pow(aresta, 3);
	}

	public double getAresta() {
		return aresta;
	}

	public void setAresta(double aresta) {
		this.aresta = aresta;
	}
	
	
}