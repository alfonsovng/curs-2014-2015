package exercici5.figures.f3d;

public class Esfera extends Figura3D {

	private double radi;
	
	public Esfera(double radi) {
		setRadi(radi);
	}
	
	public double calculaArea() {
		double area;
		area = 4 * Math.PI * Math.pow(radi, 2);
		return area;
	}
	
	public double calculaVolum() {
		double volum;
		volum = (4/3) * Math.PI * Math.pow(radi, 2);

		return volum;
	}

	public double getRadi() {
		return radi;
	}

	public void setRadi(double radi) {
		this.radi = radi;
	}
	
}