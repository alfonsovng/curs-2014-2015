package exercici5.figures.f2d;

public class Cercle extends Figura2D {

	private double radi;

	public Cercle(double radi) {
		setRadi(radi);
	}

	public double getRadi() {
		return radi;
	}

	public void setRadi(double radi) {
		this.radi = radi;
	}
	
	public double calculaPerimetre() {
		double perimetre;
		perimetre = 2*Math.PI*radi;
		return perimetre;
	}
	
	public double calculaArea() {
		double area;
		area = Math.PI*Math.pow(radi, 2);
		return area;
	}

	@Override
	public String toString() {
		return "Cercle [radi=" + radi + "]";
	}
}
