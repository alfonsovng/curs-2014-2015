package exercici5.figures.f2d;

public class Rectangle extends Figura2D {

	private double base;
	private double altura;

	public Rectangle(double base, double altura){
		setBase(base);
		setAltura(altura);
	}
	
	public double getBase() {
		return base;
	}
	
	public void setBase(double base) {
		this.base = base;
	}
	
	public double getAltura() {
		return altura;
	}
	
	public void setAltura(double altura) {
		this.altura = altura;
	}
	
	public double calculaPerimetre() {
		double perimetre;
		perimetre = 2*base + 2*altura;
		return perimetre;
	}
	
	public double calculaArea() {
		double area;
		area = base*altura;
		return area;
	}

	@Override
	public String toString() {
		return "Rectangle [base=" + base + ", altura=" + altura + "]";
	}
}
























