package exercici5.figures.f2d;

import exercici5.figures.FiguraGeometrica;

public abstract class Figura2D extends FiguraGeometrica {
	
	public abstract double calculaPerimetre();
}
