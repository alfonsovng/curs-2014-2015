package exercici5.figures;

public abstract class FiguraGeometrica {

	public enum Color { 
		VERMELL, 
		BLAU,
		GROC,
		BLANC; 
	};
	
	private Color color;
	
	public Color getColor() {
		return color;
	}

	public void setColor(Color color) {
		this.color = color;
	}

	public abstract double calculaArea();
}
