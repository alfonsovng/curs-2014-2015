## Informació ##
Repositori amb el codi de prova del mòdul de programació orientada a objectes impartit per [Alfonso da Silva](mailto:alfonsodasilva@gmail.com) 
a l'[Institut Joaquim Mir de Vilanova i la Geltrú](http://informatica.iesjoaquimmir.cat/wordpress/) durant el curs 2014-2015.

##Llicència##
Copyright (C) Alfonso da Silva (alfonsodasilva@gmail.com)

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see http://www.gnu.org/licenses/.