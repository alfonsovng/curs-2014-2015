package cat.jmir.serialitzacio;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.util.ArrayList;

/**
 * Exemple de serialització. Llegeix un objecte Persona
 * (o un array de personeS) del fitxer persona.bin i 
 * ho mostra per pantalla.
 */
public class ObjectInputStreamExemple {
	
	private static final String FILE_NAME = "/home/alfonso/persona.bin";
	
	public static void main(String[] args) {
		
		ObjectInputStream objectInput = null;
        try {
            objectInput = new ObjectInputStream(new FileInputStream(FILE_NAME));
             
//            //exemple 1: llegint 1 persona
//            Persona p = (Persona) objectInput.readObject();
//            System.out.println(p);
            
            //exemple 2: llegint un array de persones            
            ArrayList<Persona> list = (ArrayList<Persona>) objectInput.readObject();
            for(int i=0;i<list.size();++i) {
            	System.out.println(i + " --> " + list.get(i));
            }
            
            objectInput.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
        	if(objectInput != null) {
        		try {
        			objectInput.close();
        		} catch (IOException ignored) {
        		}
        	}
        }
    }
}
