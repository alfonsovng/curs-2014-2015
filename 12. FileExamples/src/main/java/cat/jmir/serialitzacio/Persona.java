package cat.jmir.serialitzacio;

import java.io.Serializable;

/**
 * Classe que es guardarà a un fitxer en format binari.
 * Ha de ser Serializable.
 */
public class Persona implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	private String nom;
    private String primerCognom;
    private String segonCognom;
    private int edat;
    private boolean esquerra;
 
    public Persona() {
    }
     
    public String getNom() {
        return nom;
    }
     
    public void setNom(String nom) {
        this.nom = nom;
    }
     
    public String getPrimerCognom() {
        return primerCognom;
    }
     
    public void setPrimerCognom(String primerCognom) {
        this.primerCognom = primerCognom;
    }
     
    public String getSegonCognom() {
        return segonCognom;
    }
     
    public void setSegonCognom(String segonCognom) {
        this.segonCognom = segonCognom;
    }
     
    public int getEdat() {
        return edat;
    }
     
    public void setEdat(int edat) {
        this.edat = edat;
    }
     
    public boolean isEsquerra() {
        return esquerra;
    }
     
    public void setEsquerra(boolean esEsquerra) {
        this.esquerra = esEsquerra;
    }
     
    public String toString() {
        return "Persona [nom=" + nom + ", primerCognom=" + primerCognom
                + ", segonCognom=" + segonCognom + ", edat=" + edat
                + ", esEsquerra=" + esquerra + "]";
    }
}