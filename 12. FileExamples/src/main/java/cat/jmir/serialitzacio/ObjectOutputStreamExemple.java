package cat.jmir.serialitzacio;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 * Exemple de serialització. Crea un objecte Persona 
 * (o un array de persones) i el guarda al fitxer persona.bin
 */
public class ObjectOutputStreamExemple {

	private static final String FILE_NAME = "/home/alfonso/persona.bin";
	
	public static void main(String[] args) {
		
        Persona p = new Persona();
        p.setNom("Alfonso");
        p.setPrimerCognom("da Silva");
        p.setSegonCognom("Saavedra");
        p.setEdat(40);
        p.setEsquerra(false);
        
        ArrayList<Persona> list = new ArrayList<Persona>();
        list.add(p);
         
        ObjectOutputStream objectOutput = null;
        try {
        	objectOutput = new ObjectOutputStream(new FileOutputStream(FILE_NAME));
            
//        	//exemple 1: escribint 1 persona
//        	objectOutput.writeObject(p);
            
        	//exemple 2: escribint una llista de persones
        	objectOutput.writeObject(list);
        	
        	System.out.println("S'ha generat el fitxer " + FILE_NAME);
        	
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
        	if(objectOutput != null) {
        		try {
        			objectOutput.close();
        		} catch (IOException ignored) {
        		}
        	}
        }
    }
}
