package cat.jmir.text;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
 
/**
 * Exemple de lectura en format text d'un fitxer.
 *
 */
public class FileReaderExemple {
 
    public static void main(String[] args) {
         
        FileReader reader = null;
 
        try {
            reader = new FileReader("/home/alfonso/doc.txt");
            /*
             * també es podria crear el reader així:
             * 
             * File file = new File("/home/institut/LICENSE");
             * reader = new FileReader(file);
             */
            int charsLlegits;
            char[] buffer = new char[1024];
            do {
                charsLlegits = reader.read(buffer);
 
                for(int i=0;i<charsLlegits;++i) {
                    System.out.print(buffer[i]);
                }
                //if(charsLlegits != -1) {
                //          System.out.print(new String(buffer, 0, charsLlegits));
                //}
 
            } while (charsLlegits != -1);
 
        } catch (FileNotFoundException e) {
            System.err.println("Fitxer no trobat: " + e);
        } catch (IOException e) {
            System.err.println("Error d'I/O: " + e);
        } finally {
            if(reader != null) {
                try {
                    reader.close();
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                }
            }
        }
    }
}