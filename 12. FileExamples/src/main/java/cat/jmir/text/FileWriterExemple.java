package cat.jmir.text;

import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
 
/**
 * Exemple d'escriptura en format text a un fitxer.
 *
 */
public class FileWriterExemple {
 
    public static void main(String[] args) {
        FileWriter writer = null;
 
        try {
            writer = new FileWriter("/home/alfonso/lala.txt");
 
            writer.write("hola\n");
            writer.write("ciao\n");
 
        } catch (FileNotFoundException e) {
            System.err.println("Fitxer no trobat: " + e);
        } catch (IOException e) {
            System.err.println("Error d'I/O: " + e);
        } finally {
            if(writer != null) {
                try {
                    writer.close();
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                }
            }
        }
    }
}