package cat.jmir.binary;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
 
/**
 * Exemple d'escriptura de fitxes en binari.
 * En aquest cas, gener un fitxer en format bmp de color blanc.
 */
public class FileOutputStreamExemple {
    
	private static final String FILE_NAME = "/home/alfonso/test.bmp";
	
    public static void main(String[] args) {
        FileOutputStream output = null;
         
        try {
            output = new FileOutputStream(FILE_NAME);
            
            BMPGenerator generator = new BMPGenerator(
            		255, 	//r 
            		0, 		//g
            		0); 	//b

            //escriu el BMP a l'output
            generator.write(output);
            
            System.out.println("S'ha generat el fitxer " + FILE_NAME);
            
        } catch (FileNotFoundException e) {
            System.err.println("Fitxer no trobat: " + e);
        } catch (IOException e) {
            System.err.println("Error d'I/O: " + e);
        } finally {
            if(output != null) {
                try {
                    output.close();
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                }
            }
        }
    }
    
}
