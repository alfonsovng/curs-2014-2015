package cat.jmir.binary;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
 
/**
 * Exemple de lectura de fitxers binaris.
 * En aquest cas, llegeix un fitxe binari i el mostra per
 * pantalla en format hexadecimal.
 */
public class FileInputStreamExemple {
 
	private static final String FILE_NAME = "/home/alfonso/test.bmp";
	
    public static void main(String[] args) {    
        InputStream input = null;
         
        try {
            input = new FileInputStream(FILE_NAME);
             
            int bytesLlegits;
            byte[] buffer = new byte[3];
            do {
                bytesLlegits = input.read(buffer);
                 
                for(int i=0;i<bytesLlegits;++i) {
                    System.out.print(Integer.toHexString(buffer[i]));
                    System.out.print(' ');
                }
                System.out.println();
                 
            } while (bytesLlegits != -1);
             
        } catch (FileNotFoundException e) {
            System.err.println("Fitxer no trobat: " + e);
        } catch (IOException e) {
            System.err.println("Error d'I/O: " + e);
        } finally {
            if(input != null) {
                try {
                    input.close();
                } catch (IOException ignored) {
                    ignored.printStackTrace();
                }
            }
        }
    }
}
