package cat.jmir.file;

import java.io.File;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

/**
 * Exemple d'us de la classe File, que proporciona
 * informació sobre fitxers i directoris.
 *
 */
public class FileExample 
{
	public static void main(String[] args) {

		Scanner input = new Scanner(System.in);

		System.out.println("Introdueix el nom del fitxer: ");

		String fileName = input.nextLine();

		File f = new File(fileName);

		System.out.println("f.exists(): " + f.exists());

		System.out.println("f.isFile(): " + f.isFile());

		System.out.println("f.isDirectory(): " + f.isDirectory());

		System.out.println("f.getAbsolutePath(): " + f.getAbsolutePath());

		System.out.println("f.getName(): " + f.getName());

		System.out.println("f.length(): " + f.length());

		System.out.println("f.lastModified(): " + f.lastModified());

		/*
		 * Aquí es proven les opcions de crear i esborrar fitxers.
		 */
		if(f.isDirectory()) {
			System.out.println("f.list(): " + Arrays.toString(f.list()));
		} else if(!f.exists()) {
			System.out.println("El vols crear? S/N");
			if(input.next().equalsIgnoreCase("S")) {
				try {
					f.createNewFile();
				} catch (IOException e) {
					System.out.println("No s'ha pogut crear el fitxer: " + e.getMessage());
				}
			}
		} else if(f.canWrite()) {
			System.out.println("El vols esborrar? S/N");
			if(input.next().equalsIgnoreCase("S")) {
				f.delete();
			}
		}
	}
}
