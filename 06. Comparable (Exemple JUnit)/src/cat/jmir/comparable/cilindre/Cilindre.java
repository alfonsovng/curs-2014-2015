package cat.jmir.comparable.cilindre;

public class Cilindre implements Comparable<Cilindre> {
	
	private final double h;
	private final double r;
	
	public Cilindre(double r, double h) {
		this.r=r;
		this.h=h;
	}
	
	public double getVolum(){
		return Math.PI*this.r*this.r*this.h;
	}

	@Override
	public int compareTo(Cilindre that) {
		if (this.getVolum()<that.getVolum())
			return -1;
		else if (this.getVolum()>that.getVolum())
			return 1;
		else
			return 0;
		
//		return (int) (that.getVolum() - this.getVolum());
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		long temp;
		temp = Double.doubleToLongBits(getVolum());
		result = prime * result + (int) (temp ^ (temp >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Cilindre))
			return false;
		Cilindre other = (Cilindre) obj;
		if (Double.doubleToLongBits(getVolum()) != Double.doubleToLongBits(other.getVolum()))
			return false;
		return true;
	}

	public String toString(){
		return "[Cilindre de radi: "+this.r+" i altura: "+this.h+"]";
	}
}
