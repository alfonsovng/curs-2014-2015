package cat.jmir.comparable.estudiant;

public class Estudiant implements Comparable<Estudiant>{
	private final String nom;
	private final String cognom1;
	private final String cognom2;
		
	public Estudiant(String nom, String cognom1, String cognom2) {
		this.nom = nom;
		this.cognom1 = cognom1;
		this.cognom2 = cognom2;
	}

	@Override
	public int compareTo(Estudiant that) {
		if(this.cognom1.compareTo(that.getCognom1())!=0)
			return this.cognom1.compareTo(that.getCognom1());
		else if (this.cognom2.compareTo(that.getCognom2())!=0)
			return this.cognom2.compareTo(that.getCognom2());
		else
			return this.nom.compareTo(that.getNom());
	}

	public String getNom() {
		return nom;
	}

	public String getCognom1() {
		return cognom1;
	}

	public String getCognom2() {
		return cognom2;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cognom1 == null) ? 0 : cognom1.hashCode());
		result = prime * result + ((cognom2 == null) ? 0 : cognom2.hashCode());
		result = prime * result + ((nom == null) ? 0 : nom.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Estudiant))
			return false;
		Estudiant other = (Estudiant) obj;
		if (cognom1 == null) {
			if (other.cognom1 != null)
				return false;
		} else if (!cognom1.equals(other.cognom1))
			return false;
		if (cognom2 == null) {
			if (other.cognom2 != null)
				return false;
		} else if (!cognom2.equals(other.cognom2))
			return false;
		if (nom == null) {
			if (other.nom != null)
				return false;
		} else if (!nom.equals(other.nom))
			return false;
		return true;
	}

	@Override
	public String toString(){
		return this.cognom1+" "+this.cognom2+", "+this.nom;
	}
}