package cat.jmir.comparable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class StringTest {

	public static void main(String[] args) {
		List<String> list=new ArrayList<String>();
		list.add("Patata");
		list.add("Xoriço");
		list.add("Aigua");
		list.add("Tomaquet");
		
		Collections.sort(list);
		
		for(String s: list){
			System.out.println(s);
		}
	}
}
