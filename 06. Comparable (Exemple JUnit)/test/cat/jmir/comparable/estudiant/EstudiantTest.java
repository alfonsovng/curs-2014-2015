package cat.jmir.comparable.estudiant;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class EstudiantTest {

	private Estudiant e1 = new Estudiant("Pedro", "García", "Lopez");
	private Estudiant e2 = new Estudiant("Roberto", "García", "Lopez");
	private Estudiant e3 = new Estudiant("Pedro", "García", "Osorio");
	private Estudiant e4 = new Estudiant("Marcos", "Serrano", "Tenorio");
	private Estudiant e5 = new Estudiant("Marcos", "Tenorio", "Tenorio");
	
	@Test
	public void testComparable() {
		Estudiant[] llistaOrdenada = new Estudiant[5];
		llistaOrdenada[0] = e1;
		llistaOrdenada[1] = e2;
		llistaOrdenada[2] = e3;
		llistaOrdenada[3] = e4;
		llistaOrdenada[4] = e5;
		
		Estudiant[] llistaDesordenada = scramble(llistaOrdenada);
				
		Arrays.sort(llistaDesordenada);
		
		assertArrayEquals(llistaOrdenada,llistaDesordenada);
	}
	
	@Test
	public void testComparableAmbRepeticions() {
		Estudiant[] llistaOrdenada = new Estudiant[9];
		llistaOrdenada[0] = e1;
		llistaOrdenada[1] = e1;
		llistaOrdenada[2] = e2;
		llistaOrdenada[3] = e3;
		llistaOrdenada[4] = e3;
		llistaOrdenada[5] = e4;
		llistaOrdenada[6] = e4;
		llistaOrdenada[7] = e4;
		llistaOrdenada[8] = e5;
		
		Estudiant[] llistaDesordenada = scramble(llistaOrdenada);
				
		Arrays.sort(llistaDesordenada);
		
		assertArrayEquals(llistaOrdenada,llistaDesordenada);
	}

	//http://www.programcreek.com/2012/02/java-method-to-shuffle-an-int-array-with-random-order/
	private Estudiant[] scramble(Estudiant[] original) {
		Estudiant[] array = new Estudiant[original.length];		
		System.arraycopy(original, 0, array, 0, original.length);
		
		Random rgen = new Random();  // Random number generator			
		 
		for (int i=0; i<array.length; i++) {
		    int randomPosition = rgen.nextInt(array.length);
		    Estudiant temp = array[i];
		    array[i] = array[randomPosition];
		    array[randomPosition] = temp;
		}
 
		return array;
	}
}
