package cat.jmir.comparable;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import cat.jmir.comparable.cilindre.CilindreTest;
import cat.jmir.comparable.estudiant.EstudiantTest;

@RunWith(Suite.class)
@SuiteClasses({
	CilindreTest.class,
	EstudiantTest.class
})
public class AllTests {
}
