package cat.jmir.comparable.cilindre;

import static org.junit.Assert.assertArrayEquals;

import java.util.Arrays;
import java.util.Random;

import org.junit.Test;

public class CilindreTest {

	private Cilindre c1 = new Cilindre(10, 20);
	private Cilindre c2 = new Cilindre(10, 21);
	private Cilindre c3 = new Cilindre(10, 22);
	private Cilindre c4 = new Cilindre(10, 23);
		
	@Test
	public void testComparable() {
		
		Cilindre[] llistaOrdenada = new Cilindre[4];
		llistaOrdenada[0] = c1;
		llistaOrdenada[1] = c2;
		llistaOrdenada[2] = c3;
		llistaOrdenada[3] = c4;
		
		Cilindre[] llistaDesordenada = scramble(llistaOrdenada);
				
		Arrays.sort(llistaDesordenada);
		
		assertArrayEquals(llistaOrdenada,llistaDesordenada);
	}
	
	@Test
	public void testComparableAmbRepeticions() {
		
		Cilindre[] llistaOrdenada = new Cilindre[7];
		llistaOrdenada[0] = c1;
		llistaOrdenada[1] = c2;
		llistaOrdenada[2] = c2;
		llistaOrdenada[3] = c2;
		llistaOrdenada[4] = c3;
		llistaOrdenada[5] = c4;
		llistaOrdenada[6] = c4;
		
		Cilindre[] llistaDesordenada = scramble(llistaOrdenada);
				
		Arrays.sort(llistaDesordenada);
		
		assertArrayEquals(llistaOrdenada,llistaDesordenada);
	}

	//http://www.programcreek.com/2012/02/java-method-to-shuffle-an-int-array-with-random-order/
	private Cilindre[] scramble(Cilindre[] original) {
		Cilindre[] array = new Cilindre[original.length];		
		System.arraycopy(original, 0, array, 0, original.length);
		
		Random rgen = new Random();  // Random number generator			
		 
		for (int i=0; i<array.length; i++) {
		    int randomPosition = rgen.nextInt(array.length);
		    Cilindre temp = array[i];
		    array[i] = array[randomPosition];
		    array[randomPosition] = temp;
		}
 
		return array;
	}
}
