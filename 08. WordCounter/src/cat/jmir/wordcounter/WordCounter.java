package cat.jmir.wordcounter;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

public class WordCounter {

	private HashMap<String,Integer> wordMap = new HashMap<String,Integer>();
	private boolean ignoreCase;
	
	
	public WordCounter(File fileText, boolean ignoreCase) throws FileNotFoundException {
		this.ignoreCase = ignoreCase;
		readScanner(new Scanner(fileText));
		
	}
	
	public WordCounter(String text, boolean ignoreCase) {
		this.ignoreCase = ignoreCase;
		readScanner(new Scanner(text));
	}
	
	private void readScanner(Scanner input) {
		input.useDelimiter("[\\s,\\.;\\n\\t]+");
		while(input.hasNext()) {
			String word = input.next();
		
			word = normalizeWord(word);
			
			if(wordMap.containsKey(word)) {
				//Quantes vegades ha sortit ja?
				int vegades = wordMap.get(word);
				//Incremento, doncs ha tornat a apareixer
				vegades++;
				//Actualitzo la info al map
				wordMap.put(word, vegades);
			} else {
				//1a vegada que apareix la paraula
				wordMap.put(word, 1);
			}
		}
	}
	
	private String normalizeWord(String word) {
		if(ignoreCase) {
			return word.toUpperCase();
		} else {
			return word;
		}
		
	}
	
	public int getOccurences(String word) {
		word = normalizeWord(word);
		if(wordMap.containsKey(word)) {
			int occurences = wordMap.get(word);
			return occurences;
		} else {
			return 0;
		}
	}
	
	public String getMaxOccurrencesWord() {
		int maxOcurrences = -1;
		String maxOccurrencesWord = null;
		
		Set<Entry<String,Integer>> entrySet = wordMap.entrySet();
		for(Entry<String,Integer> parella:entrySet) {
			
			String word = parella.getKey();
			int occurrences = parella.getValue();
			
			if(occurrences > maxOcurrences) {
				maxOcurrences = occurrences;
				maxOccurrencesWord = word;
			}
		}
		
		return maxOccurrencesWord;
	}
}
