package cat.jmir.wordcounter;

import java.io.File;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.Options;

public class WordCounterTest {

	public static void main(String[] args) throws Exception {
		Options options = new Options();

		options.addOption("h", "help", false, "mostra l'ajuda");
		options.addOption("f", "file", true, "nom del fitxer a parsejar");
		options.addOption("w", "word", true, "paraula a buscar");
		options.addOption("i", "ignoreCase", false, "ignora majúscules/minúscules (per defecte no)");

		CommandLineParser parser = new BasicParser();
		CommandLine cmd = parser.parse(options, args);
		
		if (cmd.hasOption("h")) {
			showHelp(options);
			return;
		}
		
		String fileName;
		String wordToFind;
		boolean ignoreCase = false;
		
		if(cmd.hasOption("f")) {
			fileName =  cmd.getOptionValue("f");		
		} else {
			showHelp(options);
			return;
		}
		
		if(cmd.hasOption("w")) {
			wordToFind =  cmd.getOptionValue("w");		
		} else {
			showHelp(options);
			return;
		}
		
		if(cmd.hasOption("i")) {
			ignoreCase = true;
		}
		
		
		WordCounter w = new WordCounter(new File(fileName), ignoreCase);
		System.out.println(w.getOccurences(wordToFind));
	}
	
	private static void showHelp(Options options) {
		HelpFormatter formater = new HelpFormatter();
		formater.printHelp("Main", options);
	}

}
