package cat.jmir.th;

import java.util.GregorianCalendar;
import java.util.Set;

import org.hibernate.Query;
import org.hibernate.Session;

import cat.jmir.th.dao.hibernate.HibernateUtil;
import cat.jmir.th.model.Producte;
import cat.jmir.th.model.Usuari;

public class TestUsuari {
	
	public static void main( String[] args ) {
		
		String login1 = "alfonso1";
		String login2 = "pepe1";
		
		int id1 = insereixUsuari(login1);
		int id2 = insereixUsuari(login2);
		
		creaUnProductePerUsuariAmbId(id1, "piso cuco");
		creaUnProductePerUsuariAmbId(id1, "casa amigable"); //en creo 2
		
		llistaTotsElsProductesDeUsuariAmbId(id1);
		
		mostraUsuariAmbLogin(login2);
		
		llistaTotsElsProductesOrdenatsAlfabeticamentDeUsuariAmbId(id1, "asc");
		llistaTotsElsProductesOrdenatsAlfabeticamentDeUsuariAmbId(id1, "desc");
		
		esborraUsuariAmbId(id1);
		esborraUsuariAmbId(id2);
	}
	
	private static int insereixUsuari(String login) {
		//obtinc una sessió
		Session session = HibernateUtil.getSession();

		//inicio una transacció
		session.beginTransaction();

		Usuari u = new Usuari();
		u.setNom("Alfonso da Silva " + login.hashCode());
		u.setLogin(login);
		u.setDataNaixement(new GregorianCalendar(1975, 2, 1));

		//guardo l'objecte a la BBDD (insert!)
		session.save(u);

		//commit de la transacció
		session.getTransaction().commit();

		//el recupero desprès del commit!!!
		int id = u.getId();

		//tanco la sessió
		session.close();

		return id;
	}
	
	private static void creaUnProductePerUsuariAmbId(int id, String desc) {
		//obtinc una sessió
		Session session = HibernateUtil.getSession();

		//inicio una transacció
		session.beginTransaction();

		//recupero l'usuari
		Usuari u = (Usuari) session.get(Usuari.class, id);
		
		//creo un producte
		Producte p = new Producte();
		p.setDescripcioCurta(desc);
		p.setDescripcioLlarga("hola hola hola");
		p.setMetresQuadrats(100);
		p.setPreu(1000000);
		p.setPropietari(u);
		
		//guardo l'objecte a la BBDD (insert!)
		session.save(p);

		//commit de la transacció
		session.getTransaction().commit();

		//tanco la sessió
		session.close();
	}
	
	private static void llistaTotsElsProductesDeUsuariAmbId(int id) {
		Session session = HibernateUtil.getSession();

		Usuari u = (Usuari) session.get(Usuari.class, id);
		
		Set<Producte> productes = u.getProductes();
		
		for(Producte p: productes) {
			System.out.println(p);
		}
		
		//tanco la sessió
		session.close();
	}
	
	private static void mostraUsuariAmbLogin(String login) {
		Session session = HibernateUtil.getSession();
		
		Query q = session.createQuery("from Usuari where login=:login");
		q.setString("login", login);
		
		Usuari u = (Usuari) q.uniqueResult();
		
		System.out.println(u);
		
		//tanco la sessió
		session.close();
	}
	
	private static void llistaTotsElsProductesOrdenatsAlfabeticamentDeUsuariAmbId(int id, String tipusOrdre) {
		Session session = HibernateUtil.getSession();

		//Usuari u = (Usuari) session.get(Usuari.class, id);
		
		Query q = session.createQuery("from Producte where propietari.id=:p order by descripcioCurta " + tipusOrdre);
		
		q.setParameter("p", id);
		
		System.out.println(q.list());
		
		//tanco la sessió
		session.close();
	}
	
	private static void esborraUsuariAmbId(int id) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		Usuari u = (Usuari) session.get(Usuari.class, id);
		
		Set<Producte> productes = u.getProductes();
		
		for(Producte p: productes) {
			session.delete(p);
		}
		
		session.delete(u);

		session.getTransaction().commit();
		session.close();
	}
}
