package cat.jmir.th;

import org.hibernate.Session;

import cat.jmir.th.dao.hibernate.HibernateUtil;
import cat.jmir.th.model.Producte;

/**
 * La BBDD:
 * 
 	create database house;

	create user house;
	
	grant all on house.* to 'house'@'localhost' identified by '1234';
	
	CREATE TABLE house.product (
	id INT UNSIGNED AUTO_INCREMENT PRIMARY KEY,
	descripcio_breu varchar(50) NOT NULL,
	descripcio_llarga TEXT NOT NULL,
	metres_quadrats INT,
	preu INT
	) ENGINE=InnoDB DEFAULT CHARACTER SET=utf8;
 */
public class App {
	
	public static void main( String[] args ) {
		Producte p = new Producte();
		p.setDescripcioCurta("hola");
		p.setDescripcioLlarga("hola hola hola");
		p.setMetresQuadrats(100);
		p.setPreu(1000000);
		
		//insereixo el producte a la BBDD
		insertProducte(p);
		
		//llista tots els productes de la BBDD
		//TODO fent servir Query o Criteria

		//llista tots els productes de la BBDD amb més de 90 metres quadrats
		//TODO fent servir Query o Criteria

		//recupera el producte amb id 2 (o un que existeixi)
		//TODO fent servir session.load
				
		//modifica el producte anterior (canvia la descripció curta( i actualitza l'informació a la BBDD
		//TODO fent servir session.update
		
		//esborra un producte
		//TODO fent servir session.delete
	}
	
	private static void insertProducte(Producte p) {
		//obtinc una sessió
		Session session = HibernateUtil.getSession();
		
		//inicio una transacció
		session.beginTransaction();

		//guardo l'objecte a la BBDD (insert!)
		session.save(p);

		//commit de la transacció
		session.getTransaction().commit();
		
		//tanco la sessió
		session.close();
	}
}
