package cat.jmir.th.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@Table(name="producte")
public class Producte implements Serializable {

    @Id
    @GeneratedValue
    @Column(name="pk")
	private int id;
    
    @Column(nullable=false)
	private String descripcioCurta;
    
    @Column
	private String descripcioLlarga;
    
    @Column
	private int metresQuadrats;
    
    @Column
	private int preu;
    
    @Column(columnDefinition="BOOLEAN")
    private boolean ascensor;
	
    @ManyToOne
    @JoinColumn(name="propietari_id")
    private Usuari propietari;
    
	public Producte() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcioCurta() {
		return descripcioCurta;
	}

	public void setDescripcioCurta(String descripcioCurta) {
		this.descripcioCurta = descripcioCurta;
	}

	public String getDescripcioLlarga() {
		return descripcioLlarga;
	}

	public void setDescripcioLlarga(String descripcioLlarga) {
		this.descripcioLlarga = descripcioLlarga;
	}

	public int getMetresQuadrats() {
		return metresQuadrats;
	}

	public void setMetresQuadrats(int metresQuadrats) {
		this.metresQuadrats = metresQuadrats;
	}

	public int getPreu() {
		return preu;
	}

	public void setPreu(int preu) {
		this.preu = preu;
	}

	public boolean isAscensor() {
		return ascensor;
	}

	public void setAscensor(boolean ascensor) {
		this.ascensor = ascensor;
	}

	public Usuari getPropietari() {
		return propietari;
	}

	public void setPropietari(Usuari propietari) {
		this.propietari = propietari;
	}

	@Override
	public String toString() {
		return "Producte [id=" + id + ", descripcioCurta=" + descripcioCurta
				+ ", descripcioLlarga=" + descripcioLlarga
				+ ", metresQuadrats=" + metresQuadrats + ", preu=" + preu
				+ ", ascensor=" + ascensor + "]";
	}
}
