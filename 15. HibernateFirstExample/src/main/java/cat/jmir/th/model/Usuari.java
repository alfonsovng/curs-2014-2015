package cat.jmir.th.model;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Type;

@Entity
@Table(name="usuari")
public class Usuari implements Serializable {

	@Id
	@GeneratedValue
	@Column(name="pk")
	private int id;
	
	@Column
	@Type(type="calendar_date")
	private Calendar dataNaixement;

	@Column(nullable=false)
	private String nom;

	@Column(nullable=false,unique=true)
	private String login;
	
	@OneToMany(mappedBy="propietari")
	private Set<Producte> productes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Calendar getDataNaixement() {
		return dataNaixement;
	}

	public void setDataNaixement(Calendar dataNaixement) {
		this.dataNaixement = dataNaixement;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Set<Producte> getProductes() {
		return productes;
	}

	public void setProductes(Set<Producte> productes) {
		this.productes = productes;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	@Override
	public String toString() {
		return "Usuari [id=" + id + ", nom=" + nom + ", login=" + login + "]";
	}
}
