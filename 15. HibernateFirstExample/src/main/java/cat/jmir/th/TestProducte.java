package cat.jmir.th;

import java.util.List;

import org.hibernate.Query;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.criterion.Restrictions;

import cat.jmir.th.dao.hibernate.HibernateUtil;
import cat.jmir.th.model.Producte;

public class TestProducte {

	public static void main( String[] args ) {

		//insereixo el producte a la BBDD
		int id = insereixProducte();

		//llista tots els productes de la BBDD
		llistaTotsElsProductes();

		//llista tots els productes de la BBDD amb més de 90 metres quadrats
		llistaTotsElsProductesAmbMesDe90MetresQuadratsAmbCriteria();

		//llista tots els productes de la BBDD amb més de 90 metres quadrats
		llistaTotsElsProductesAmbMesDe90MetresQuadratsAmbQuery();
		
		//modifica el producte anterior (canvia la descripció curta( i actualitza l'informació a la BBDD
		modificaProducteAmbId(id);

		//recupera el producte amb id 2 (o un que existeixi)
		recuperaProducteAmbId(id);

		//esborra un producte
		esborraProducteAmbId(id);
	}

	private static int insereixProducte() {
		//obtinc una sessió
		Session session = HibernateUtil.getSession();

		//inicio una transacció
		session.beginTransaction();

		Producte p = new Producte();
		p.setDescripcioCurta("hola");
		p.setDescripcioLlarga("hola hola hola");
		p.setMetresQuadrats(100);
		p.setPreu(1000000);

		//guardo l'objecte a la BBDD (insert!)
		session.save(p);

		//commit de la transacció
		session.getTransaction().commit();

		//el recupero desprès del commit!!!
		int id = p.getId();

		//tanco la sessió
		session.close();

		return id;
	}

	private static void llistaTotsElsProductes() {
		Session session = HibernateUtil.getSession();

		List<Producte> list = session.createCriteria(Producte.class).list();

		for(Producte p: list) {
			System.out.println(p);
		}

		session.close();
	}

	private static void llistaTotsElsProductesAmbMesDe90MetresQuadratsAmbCriteria() {
		Session session = HibernateUtil.getSession();

		Criteria c = session.createCriteria(Producte.class);
		c.add(Restrictions.gt("metresQuadrats", 90));

		List<Producte> list = c.list();

		for(Producte p: list) {
			System.out.println(p);
		}

		session.close();
	}
	
	private static void llistaTotsElsProductesAmbMesDe90MetresQuadratsAmbQuery() {
		Session session = HibernateUtil.getSession();

		Query q = session.createQuery("from Producte where metresQuadrats >= :m");
		q.setInteger("m", 90);

		List<Producte> list = q.list();

		for(Producte p: list) {
			System.out.println(p);
		}

		session.close();
	}

	private static void recuperaProducteAmbId(int id) {
		Session session = HibernateUtil.getSession();

		Producte p = (Producte) session.get(Producte.class, id);
		System.out.println(p);

		session.close();
	}

	private static void modificaProducteAmbId(int id) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		Producte p = (Producte) session.get(Producte.class, id);
		p.setMetresQuadrats(p.getMetresQuadrats() + 1);

		session.save(p);

		session.getTransaction().commit();
		session.close();
	}

	private static void esborraProducteAmbId(int id) {
		Session session = HibernateUtil.getSession();
		session.beginTransaction();

		Producte p = (Producte) session.get(Producte.class, id);
		session.delete(p);

		session.getTransaction().commit();
		session.close();
	}
}
