package cat.jmir.nextday;

/**
 * Classe que permet calcular el dia següent a una data donada
 * 
 * @author alfonso
 * @see #calcula(int, int, int)
 */
public class NextDay {

	/**
	 * Comprova si l'any <code>y</code> és de traspàs
	 * 
	 * @param y any
	 * @return true si és any de traspàs
	 */
	boolean esAnyDeTraspas(int y) {
		return y % 400 == 0 || (y % 4 == 0 && y % 100 != 0);
	}

	
	/**
	 * Retorna el darrer dia del mes (30, 31) o, en el cas de febrer, 
	 * 28 o 29 depenent de si és any de traspàs
	 * 
	 * @param m mes (valor entre 1 i 12)
	 * @param y any
	 * @return el darrer dia del mes
	 * @throws IllegalArgumentException si el mes es incorrecte
	 * @see #esAnyDeTraspas(int)
	 */
	private int darrerDiaMes(int m, int y) {
		if (m == 4 || m == 6 || m == 9 || m == 11) {
			return 30;
		} else if (m == 1 || m == 3 || m == 5 || m == 7 || m == 8 || m == 10 || m == 12) {
			return 31;
		} else if (m == 2) {
			if(esAnyDeTraspas(y)) {
				return 29;
			} else {
				return 28;
			}
		}
		throw new IllegalArgumentException("Més incorrecte: " + m);
	}
	
	/**
	 * Retorna en forma de <code>String</code> el dia següent al dia
	 * donat.
	 * 
	 * @param d dia 
	 * @param m mes 
	 * @param y any
	 * @return el dia següent al donat, en format d/m/y
	 * @throws IllegalArgumentException si el dia o el mes són incorrectes
	 */
	public String calcula(int d, int m, int y) {
		int darrerDiaMes = darrerDiaMes(m, y);
		
		if(d < 1 || d > darrerDiaMes) {
			throw new IllegalArgumentException("Dia incorrecte: " + m);
		}
		
		//canvi de dia
		d++;
		if(d > darrerDiaMes) {
			//canvi de més
			d = 1;
			m++;
			if(m > 12) {
				//canvi d'any
				m = 1;
				y++;
			}
		}
		
		return d + "/" + m + "/" + y;
	} 
}
