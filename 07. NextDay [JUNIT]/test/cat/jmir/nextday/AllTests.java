package cat.jmir.nextday;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AnyDeTraspasTest.class, DiaSeguentTest.class })
public class AllTests {

}
