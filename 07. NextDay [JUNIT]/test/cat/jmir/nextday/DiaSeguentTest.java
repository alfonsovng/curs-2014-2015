package cat.jmir.nextday;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import cat.jmir.nextday.NextDay;

public class DiaSeguentTest {

	NextDay nextDay;

	@Before
	public void init() {
		this.nextDay = new NextDay();
	}

	@Test
	public void testJanuaryNormalDay() {
		String result = nextDay.calcula(30, 1, 2011);
		assertEquals("31/1/2011", result);
	}

	@Test
	public void testJanuaryLastDay() {
		String result = nextDay.calcula(31, 1, 2011);
		assertEquals("1/2/2011", result);
	}

	@Test
	public void testFebruaryNormalDay() {
		String result = nextDay.calcula(27, 2, 2011);
		assertEquals("28/2/2011", result);
	}

	@Test
	public void testFebruaryLastDayNotLeapYear() {
		String result = nextDay.calcula(28, 2, 2011);
		assertEquals("1/3/2011", result);
	}

	@Test
	public void testFebruaryLastDayLeapYear() {
		String result = nextDay.calcula(29, 2, 2012);
		assertEquals("1/3/2012", result);
	}

	@Test
	public void testFebruaryPenultimateDayLeapYear() {
		String result = nextDay.calcula(28, 2, 2012);
		assertEquals("29/2/2012", result);
	}

	@Test
	public void testMarchNormalDay() {
		String result = nextDay.calcula(30, 3, 2011);
		assertEquals("31/3/2011", result);
	}

	@Test
	public void testMarchLastDay() {
		String result = nextDay.calcula(31, 3, 2011);
		assertEquals("1/4/2011", result);
	}

	@Test
	public void testAprilNormalDay() {
		String result = nextDay.calcula(29, 4, 2011);
		assertEquals("30/4/2011", result);
	}

	@Test
	public void testAprilLastDay() {
		String result = nextDay.calcula(30, 4, 2011);
		assertEquals("1/5/2011", result);
	}

	@Test
	public void testMayNormalDay() {
		String result = nextDay.calcula(30, 5, 2011);
		assertEquals("31/5/2011", result);
	}

	@Test
	public void testMayLastDay() {
		String result = nextDay.calcula(31, 5, 2011);
		assertEquals("1/6/2011", result);
	}

	@Test
	public void testJuneNormalDay() {
		String result = nextDay.calcula(29, 6, 2011);
		assertEquals("30/6/2011", result);
	}

	@Test
	public void testJuneLastDay() {
		String result = nextDay.calcula(30, 6, 2011);
		assertEquals("1/7/2011", result);
	}

	@Test
	public void testJulyNormalDay() {
		String result = nextDay.calcula(30, 7, 2011);
		assertEquals("31/7/2011", result);
	}

	@Test
	public void testJulyLastDay() {
		String result = nextDay.calcula(31, 7, 2011);
		assertEquals("1/8/2011", result);
	}

	@Test
	public void testAugustNormalDay() {
		String result = nextDay.calcula(30, 8, 2011);
		assertEquals("31/8/2011", result);
	}

	@Test
	public void testAugustLastDay() {
		String result = nextDay.calcula(31, 8, 2011);
		assertEquals("1/9/2011", result);
	}

	@Test
	public void testSeptemberNormalDay() {
		String result = nextDay.calcula(29, 9, 2011);
		assertEquals("30/9/2011", result);
	}

	@Test
	public void testSeptemberLastDay() {
		String result = nextDay.calcula(30, 9, 2011);
		assertEquals("1/10/2011", result);
	}

	@Test
	public void testOctoberNormalDay() {
		String result = nextDay.calcula(30, 10, 2011);
		assertEquals("31/10/2011", result);
	}

	@Test
	public void testOctoberLastDay() {
		String result = nextDay.calcula(31, 10, 2011);
		assertEquals("1/11/2011", result);
	}

	@Test
	public void testNovemberNormalDay() {
		String result = nextDay.calcula(29, 11, 2011);
		assertEquals("30/11/2011", result);
	}

	@Test
	public void testNovemberLastDay() {
		String result = nextDay.calcula(30, 11, 2011);
		assertEquals("1/12/2011", result);
	}

	@Test
	public void testDecemberNormalDay() {
		String result = nextDay.calcula(30, 12, 2011);
		assertEquals("31/12/2011", result);
	}

	@Test
	public void testDecemberLastDay() {
		String result = nextDay.calcula(31, 12, 2011);
		assertEquals("1/1/2012", result);
	} 
}
