package cat.jmir.nextday;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Before;
import org.junit.Test;

public class AnyDeTraspasTest {

	NextDay nextDay;

	@Before
	public void init() {
		this.nextDay = new NextDay();
	}
	
	@Test
	public void testYearMultiple400() {	
		boolean result = nextDay.esAnyDeTraspas(2000);
		assertTrue(result);	
	}

	@Test
	public void testYearMultiple4NotMultiple100() {	
		boolean result = nextDay.esAnyDeTraspas(2012);
		assertTrue(result);	
	}

	@Test
	public void testYearMultiple4Multiple100NotMultiple400() {	
		boolean result = nextDay.esAnyDeTraspas(1700);
		assertFalse(result);	
	}

	@Test
	public void testYearNotMultiple4Multiple100NotMultiple400() {	
		boolean result = nextDay.esAnyDeTraspas(2200);
		assertFalse(result);	
	}

	@Test
	public void testYearNotMultiple4NotMultiple100NotMultiple400() {	
		boolean result = nextDay.esAnyDeTraspas(1999);
		assertFalse(result);
	}

}
