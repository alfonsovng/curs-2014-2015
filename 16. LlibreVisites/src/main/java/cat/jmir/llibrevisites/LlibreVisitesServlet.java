package cat.jmir.llibrevisites;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

public class LlibreVisitesServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	private Logger log = Logger.getLogger(LlibreVisitesServlet.class);

	@Override
	public void init()  {
		log.info("Serlvet iniciat");
	}
	
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.getSession().setAttribute("missatge", "Hola des del servlet!");
		
		getServletConfig().getServletContext().getRequestDispatcher("/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}








