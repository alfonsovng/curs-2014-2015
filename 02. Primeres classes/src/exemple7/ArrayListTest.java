package exemple7;

import java.util.ArrayList;

public class ArrayListTest {

	public static void main(String[] args) {
		
		ArrayList<String> list = new ArrayList<String>();
		
		//add(Element e): afegeix l'element e al final de la llista
		list.add("pepe");
		list.add("paco");		
		list.add("pocholo");
		showList(list);

		//add(int index, Element e): afegeix l'element e a la posició index, desplaçant els altres cap a la dreta.
		list.add(1, "periquito");
		showList(list);
		
		//set(int index, Element e): substitueix l'ement de la posició index per l'element e.
		list.set(0, "pachuli");
		showList(list);
		
		//remove(int index): igual que l'anterior, però a més el treu de la llista.Els elements és desplacen cap a l'esquerra per ocupar el lloc buit.
		list.remove(2);
		showList(list);
		
		//contains(Element e): retorna true si l'element e és a la llista. Les comparacions les fa fent servir el mètode equals.
		System.out.println("Is pocholo here? " + list.contains("pocholo"));
		System.out.println("Is farruquito here? " + list.contains("farruquito"));
		System.out.println();
		
		//indexOf(Element e): retorna la posición on hi és l'element e a la llista o -1 si no hi és. Les comparacions les fa fent servir el mètode equals.
		System.out.println("At what position pocholo is? " + list.indexOf("pocholo"));
		System.out.println("At what position farruquito is? " + list.indexOf("farruquito"));
		System.out.println();
		
		list.clear();
		showList(list);
	}
	
	private static void showList(ArrayList<String> list) {
		//size(): retorna la quantitat d'elements que té la llista
		System.out.println("La mida de l'array és: " + list.size());

		//isEmpty(): retorna true si size() és 0.
		System.out.println("Aquesta array és buit?: " + list.isEmpty());
		
		//size(): retorna la quantitat d'elements que té la llista
		for(int i=0;i<list.size();++i) {
			//get(int index): recupera l'element de la posició index
			System.out.println("\tElement " + i + ": " + list.get(i));
		}
		System.out.println("-------------");
		System.out.println();
	}	
}
