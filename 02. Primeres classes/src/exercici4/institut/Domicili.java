package exercici4.institut;

public class Domicili {

	private String via;
	private String numero;
	private String pis;
	private String codiPostal;
	private String poblacio;
	private String provincia;
	
	public Domicili() {
	}
	
	public Domicili(String via, String numero, String pis, String codiPostal,
			String poblacio, String provincia) {
		super();
		this.via = via;
		this.numero = numero;
		this.pis = pis;
		this.codiPostal = codiPostal;
		this.poblacio = poblacio;
		this.provincia = provincia;
	}



	public String getVia() {
		return via;
	}
	
	public void setVia(String via) {
		this.via = via;
	}
	
	public String getNumero() {
		return numero;
	}
	
	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getPis() {
		return pis;
	}
	
	public void setPis(String pis) {
		this.pis = pis;
	}
	
	public String getCodiPostal() {
		return codiPostal;
	}
	
	public void setCodiPostal(String codiPostal) {
		this.codiPostal = codiPostal;
	}
	
	public String getPoblacio() {
		return poblacio;
	}

	public void setPoblacio(String poblacio) {
		this.poblacio = poblacio;
	}

	public String getProvincia() {
		return provincia;
	}
	
	public void setProvincia(String provincia) {
		this.provincia = provincia;
	}
	
	public String getDomiciliAmigable() {
		return via + " " + numero + ", " 
				+ pis + ", " + codiPostal + ", " 
				+ poblacio + " (" + provincia + ")";
	}
	
	@Override
	public String toString() {
		return "Domicili [via=" + via + ", numero=" + numero + ", pis=" + pis
				+ ", codiPostal=" + codiPostal + ", provincia=" + provincia
				+ "]";
	}
}
