package exercici4.institut;

public class Modul {

	private String nom;
	private String descripcio;
	private int hores;
	
	public Modul(String nom, String descripcio, int hores) {
		this.nom = nom;
		this.descripcio = descripcio;
		this.hores = hores;
	}

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getDescripcio() {
		return descripcio;
	}
	public void setDescripcio(String descripcio) {
		this.descripcio = descripcio;
	}
	public int getHores() {
		return hores;
	}
	public void setHores(int hores) {
		this.hores = hores;
	}
	
	@Override
	public String toString() {
		return "Modul [nom=" + nom + ", descripcio=" + descripcio + ", hores="
				+ hores + "]";
	}
}
