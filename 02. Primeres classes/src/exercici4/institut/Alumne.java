package exercici4.institut;

import java.util.ArrayList;

public class Alumne {
	
	private static final int MAJOR_EDAT = 18;
	
	private String nom;
	private String primerCognom;
	private String segonCognom;
	private String dni;
	private ArrayList<String> telefons = new ArrayList<String>();
	private ArrayList<Modul> moduls = new ArrayList<Modul>();
	private int edat;
	private Domicili domicili;
	
	public Alumne() {
	}
	
	public Alumne(String dni) {
		this.dni = dni;
	}

	public String getNom() {
		return nom;
	}
	
	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getPrimerCognom() {
		return primerCognom;
	}
	
	public void setPrimerCognom(String primerCognom) {
		this.primerCognom = primerCognom;
	}
	
	public String getSegonCognom() {
		return segonCognom;
	}
	
	public void setSegonCognom(String segonCognom) {
		this.segonCognom = segonCognom;
	}
	
	public String getDni() {
		return dni;
	}
	
	public void setDni(String dni) {
		this.dni = dni;
	}

	public void addModul(Modul m) {
		moduls.add(m);
	}
	
	public Modul getModul(int pos) {
		return moduls.get(pos);
	}

	public void addTelefon(String t) {
		telefons.add(t);
	}

	public String getTelefon(int pos) {
		return telefons.get(pos);
	}

	public int getEdat() {
		return edat;
	}
	
	public void setEdat(int edat) {
		this.edat = edat;
	}
	
	public Domicili getDomicili() {
		return domicili;
	}
	
	public void setDomicili(Domicili domicili) {
		this.domicili = domicili;
	}

	
	public String getNomComplert() {
		return primerCognom + " " + segonCognom + ", " + nom;
	}
	
	public boolean isMajorEdat() {
		return edat >= MAJOR_EDAT;
	}

	@Override
	public String toString() {
		return "Alumne [nom=" + nom + ", primerCognom=" + primerCognom
				+ ", segonCognom=" + segonCognom + ", dni=" + dni
				+ ", telefons=" + telefons + ", moduls=" + moduls + ", edat="
				+ edat + ", domicili=" + domicili + "]";
	}

	
}
