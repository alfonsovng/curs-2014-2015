package exercici4;

import exercici4.institut.Alumne;
import exercici4.institut.Domicili;
import exercici4.institut.Modul;

public class InstitutTest {

	public static void main(String[] args) {
		Alumne alumne = new Alumne();
		alumne.setNom("Pedro");
		alumne.setPrimerCognom("García");
		alumne.setSegonCognom("Ramírez");
		alumne.setDni("55655678J");
		alumne.addTelefon("94343433434");
		alumne.addTelefon("94343433435");
		alumne.addTelefon("94343433436");
		alumne.addModul(new Modul("M03", "Programacio", 250));
		alumne.addModul(new Modul("M02", "Base de dades", 150));
		alumne.addModul(new Modul("M01", "Sistemes operatius", 432));
		alumne.addModul(new Modul("M04", "Llenguatges de Marques", 111));
		alumne.addModul(new Modul("M05", "Entorns", 250));
		alumne.setEdat(21);
		alumne.setDomicili(new Domicili("Industria", "188"
				, "2n 1a", "08800", "Badalona", "Barcelona"));
		
		System.out.println(alumne.getTelefon(0));
		System.out.println(alumne.getTelefon(1));
		System.out.println(alumne.getTelefon(2));
		
		System.out.println(alumne.getModul(0));
		System.out.println(alumne.getModul(1));
		System.out.println(alumne.getModul(2));
		System.out.println(alumne.getModul(3));
		System.out.println(alumne.getModul(4));
		System.out.println(alumne.getModul(5));
		
		//mostra el format cognom1 cognom2, nom
		System.out.println(alumne.getNomComplert());
		
		//mostra si l'alumne és major d'edat
		System.out.println(alumne.isMajorEdat());	
		
		//Pinta el mateix que la linia anterior
		System.out.println(alumne.getDomicili().getDomiciliAmigable());
		
		alumne.getDomicili().setPis("4o 6a");
		System.out.println(alumne);
	}
}
