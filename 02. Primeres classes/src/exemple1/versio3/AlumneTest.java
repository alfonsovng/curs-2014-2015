package exemple1.versio3;

public class AlumneTest {
	
	public static void main(String[] args) {
		
		//crea un objecte Alumne i l'assigna a la variable alumne1
		Alumne alumne1 = new Alumne();
		//crida el mètode setNom per assignar un nom a l'alumne1
		alumne1.setNom("Pedro");
		//crida el mètode saluda()
		alumne1.saluda();
		
		//crea un objecte Alumne i l'assigna a la variable alumne2
		Alumne alumne2 = new Alumne();
		//crida el mètode setNom per assignar un nom a l'alumne2
		alumne2.setNom("María");
		//crida el mètode saluda()
		alumne2.saluda();
		
		//crea un objecte Alumne i l'assigna a la variable alumne3
		Alumne alumne3 = new Alumne();
		//crida el mètode saluda() per veure quin nom mostra
		alumne3.saluda();
	}
}
