package exemple1.versio3;

public class Alumne {
	
	//variable d'instància del tipus String
	private String nom;
	
	//mètode SET per a assignar un nom a l'alumne
	public void setNom(String nouNom) {
		nom = nouNom;
	}
	
	//mètode GET per a recuperar el nom de l'alumne
	public String getNom() {
		return nom;
	}

	/**
	 * Mostra un missatge de salutació per pantalla
	 */
	public void saluda() {
		//fa servir la variable d'instància nom per a generar la salutació
		System.out.println("Hola, sóc un alumne i em dic " + nom);
		
//		//també es podria haver fet així
//		System.out.println("Hola, sóc un alumne i em dic " + getNom());
	}
}
 