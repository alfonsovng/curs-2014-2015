package exemple1.versio2;

import java.util.Scanner;

public class AlumneTest {
	
	public static void main(String[] args) {
		
		//crea un objecte Alumne i l'assigna a la variable alumne1
		Alumne alumne1 = new Alumne();

		//crea un objecte Scanner i l'assigna a la variable input
		Scanner input = new Scanner(System.in);
		
		//li dic a l'usuari que vull que faci
		System.out.println("Introdueix el nom de l'alumne: ");
		
		//llegeixo el text de l'usuari i el guardo a la variable nomAlumne
		String nomAlumne = input.nextLine();
		
		//faig un salt de línia per a que quedi més maco
		System.out.println();
		
		//crida el mètode saluda()
		alumne1.saluda(nomAlumne);
	}
}
