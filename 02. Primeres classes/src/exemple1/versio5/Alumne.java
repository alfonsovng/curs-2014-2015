package exemple1.versio5;

public class Alumne {
	
	private int edat;
	private String nom;
	
	//Constructor per crear un alumne amb nom
	public Alumne(String nomAlumne) {
		nom = nomAlumne;
		edat = 18; //li poso un valor per defecte
	}
	
	//Constructor per crear un alumne sense res
	public Alumne() {
	}
	
	//Constructor per crear un alumne amb nom i edat
	public Alumne(String nomAlumne, int edatAlumne) {
		nom = nomAlumne;
		edat = edatAlumne;
	}
	
	public int getEdat() {
		return edat;
	}

	public void setEdat(int novaEdat) {
		edat = novaEdat;
	}

	public void setNom(String nouNom) {
		nom = nouNom;
	}
	
	public String getNom() {
		return nom;
	}
	
	public void saluda() {		
		System.out.println("Hola, sóc en/la " + nom 
				+ " i tinc " + edat + " anys");
	}
}
