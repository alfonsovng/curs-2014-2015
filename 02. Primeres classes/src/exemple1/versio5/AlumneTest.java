package exemple1.versio5;

public class AlumneTest {
	
	public static void main(String[] args) {
		
		Alumne alumne1 = new Alumne("Victor");
		alumne1.setEdat(45);
		alumne1.saluda();
		
		Alumne alumne2 = new Alumne("Verònica", 18);
		alumne2.saluda();
		
		Alumne alumne3 = new Alumne();
		alumne3.saluda();
	}
}
