package exemple1.versio6;

public class AlumneTest {
	
	public static void main(String[] args) {
		testAlumne1();
		testAlumne2();
		testAlumne3();
	}
	
	private static void testAlumne1() {
		Alumne alumne = new Alumne("Victor");
		alumne.setEdat(45);
		alumne.saluda();
	}
	
	private static void testAlumne2() {
		Alumne alumne = new Alumne("Verònica", 18);
		alumne.saluda();
	}
	
	private static void testAlumne3() {
		Alumne alumne = new Alumne();
		alumne.saluda();
	}
}
