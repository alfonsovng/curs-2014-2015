package exemple1.versio6;

public class Alumne {
	
	private int edat;
	private String nom;
	
	public Alumne(String nom) {
		this(nom, 18); //crido al 3r constructor
	}
	
	public Alumne() {
		this("Anonymous"); //crido al 1r constructor
	}
	
	public Alumne(String nom, int edat) {
		this.setNom(nom);
		this.setEdat(edat);
	}
	
	public int getEdat() {
		return this.edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return this.nom;
	}
	
	public void saluda() {		
		System.out.println("Hola, sóc en/la " + this.getNom() 
				+ " i tinc " + this.getEdat() + " anys");
	}
}
