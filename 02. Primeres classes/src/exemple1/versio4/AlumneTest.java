package exemple1.versio4;

public class AlumneTest {
	
	public static void main(String[] args) {
		
		Alumne alumne1 = new Alumne();
		alumne1.setNom("Victor");
		alumne1.setEdat(45);
		alumne1.saluda();
		
		Alumne alumne2 = new Alumne();
		alumne2.setNom("Verònica");
		alumne2.setEdat(18);
		alumne2.saluda();
		
		Alumne alumne3 = new Alumne();
		alumne3.saluda();
	}
}
