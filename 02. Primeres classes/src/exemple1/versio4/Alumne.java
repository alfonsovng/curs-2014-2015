package exemple1.versio4;

public class Alumne {
	
	//variable d'instància del tipus int
	private int edat = 18;
	
	//variable d'instància del tipus String
	private String nom;
	
	//mètode GET per a recuperar l'edat de l'alumne
	public int getEdat() {
		return edat;
	}

	//mètode SET per a assignar una edat a l'alumne
	public void setEdat(int novaEdat) {
		edat = novaEdat;
	}

	//mètode SET per a assignar un nom a l'alumne
	public void setNom(String nouNom) {
		nom = nouNom;
	}
	
	//mètode GET per a recuperar el nom de l'alumne
	public String getNom() {
		return nom;
	}
	
	public void saluda() {		
		System.out.println("Hola, sóc en/la " + nom + " i tinc " + edat + " anys");
//		System.out.println("Hola, sóc en/la " + getNom() 
//			+ " i tinc " + getEdat() + " anys");
	}
}
