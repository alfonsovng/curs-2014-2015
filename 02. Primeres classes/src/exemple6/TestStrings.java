package exemple6;

/**
 * @see http://docs.oracle.com/javase/tutorial/java/data/strings.html
 */
public class TestStrings {

	public static void main(String[] args) {
		char[] helloArray = { 'h', 'e', 'l', 'l', 'o', '.' };
		String helloString = new String(helloArray);
		System.out.println(helloString);
		
		String stringVar = "Caracola";
		int len = stringVar.length();
		
		System.out.println("La longitud de '" + stringVar + "' és " + len);

		float floatVar = 0.22222222222222222f;
		int intVar = 98034908;
		
		//http://docs.oracle.com/javase/7/docs/api/java/util/Formatter.html
		System.out.printf("The value of the float " +
                "variable is %f, while " +
                "the value of the " + 
                "integer variable is %d, " +
                "and the string is %s", 
                floatVar, intVar, stringVar); 
	}
}
