package exemple6;

/**
 * @see http://stackoverflow.com/questions/6558285/strings-of-same-value-in-java
 */
public class TestComparacioStrings {

	public static void main(String[] args) {
		String a = "hola";
		String b = "hola";
		String c = new String("hola");
		String d = "ho" + "la";
		String e = "ho";
		e = e + "la";
				
		System.out.println(a == b);
		System.out.println(a == c);
		System.out.println(a == d);
		System.out.println(a == e);
		
		System.out.println("----------");
		
		System.out.println(a.equals(b));
		System.out.println(a.equals(c));
		System.out.println(a.equals(d));
		System.out.println(a.equals(e));
		
		System.out.println("----------");
		
		switch(c) {
		case "hola":
			System.out.println("Funciona com a paràmetre de switch!");
			break;
		default:
			System.out.println("No funciona ;(");
			break;
		}
	}
}
