package exercici3.versio2.elements;

public class Quadrat extends Figura2D {
	
	private double costat;
	
	public Quadrat(double costat){
		setCostat(costat);
	}

	public double getCostat() {
		return costat;
	}

	public void setCostat(double costat) {
		this.costat = costat;
	}
	
	public double calculaPerimetre() {
		double perimetre;
		perimetre = 4*costat;
		return perimetre;
	}
	
	public double calculaArea() {
		double area;
		area = Math.pow(costat, 2);
		return area;
	}

	@Override
	public String toString() {
		return "Quadrat [costat=" + costat + "]";
	}
}
