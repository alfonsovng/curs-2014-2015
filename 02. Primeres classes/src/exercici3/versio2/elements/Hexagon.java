package exercici3.versio2.elements;

public class Hexagon extends Figura2D {

	private double costat;

	public Hexagon(double costat){
		setCostat(costat);
	}
	
	public double getCostat() {
		return costat;
	}

	public void setCostat(double costat) {
		this.costat = costat;
	}
	
	public double calculaPerimetre() {
		double perimetre;
		perimetre = 6*costat;
		return perimetre;
	}
	
	private double calculaApotema() {
		return Math.sqrt( Math.pow(costat,2) - Math.pow(costat/2,2) );
	}
	
	public double calculaArea() {
		double area;
		area = (calculaPerimetre()*calculaApotema())/2;
		return area;
	}

	@Override
	public String toString() {
		return "Hexagon [costat=" + costat + "]";
	}
}
