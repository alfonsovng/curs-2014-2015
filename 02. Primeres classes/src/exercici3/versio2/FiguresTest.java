package exercici3.versio2;

import exercici3.versio2.elements.Cercle;
import exercici3.versio2.elements.Figura2D;
import exercici3.versio2.elements.Hexagon;

public class FiguresTest {
	
	public static void main(String[] args) {
	
		Figura2D[] figures = llegeixFigures();
		
		double sumaArees = 0;
		for(int i=0;i<figures.length;++i) {
			sumaArees+=figures[i].calculaArea();
		}
		
		System.out.println("La suma de les àrees és " + sumaArees);
	}
	
	private static Figura2D[] llegeixFigures() {
		Cercle c = new Cercle(22);
		Hexagon h = new Hexagon(33);
		
		Figura2D[] figures = new Figura2D[2];
		figures[0] = c;
		figures[1] = h;
		
		return figures;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
