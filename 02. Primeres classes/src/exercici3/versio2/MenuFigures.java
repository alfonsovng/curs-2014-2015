package exercici3.versio2;

import java.util.Scanner;

import exercici3.versio2.elements.Cercle;
import exercici3.versio2.elements.Hexagon;
import exercici3.versio2.elements.Quadrat;
import exercici3.versio2.elements.Rectangle;

public class MenuFigures {
	
	private static final String OPCIO_CERCLE = "cercle";
	private static final String OPCIO_QUADRAT = "quadrat";
	private static final String OPCIO_RECTANGLE = "rectangle";
	private static final String OPCIO_HEXAGON = "hexagon";
	private static final String OPCIO_SORTIR = "sortir";
	
	private Scanner input;
	
	public MenuFigures() {
		 input = new Scanner(System.in);
	}

	public void iniciaMenu() {
		String opcio;
		String missatge;
		boolean sortir = false;
		do {
			System.out.println("Quina figura vols? Les opcions possibles són cercle, quadrat, rectangle, hexagon o sortir si vols sortir");
			opcio=input.nextLine();
			
			switch(opcio){
				case OPCIO_CERCLE:
					missatge = testCercle();
					break;
				case OPCIO_QUADRAT:
					missatge = testQuadrat();
					break;
				case OPCIO_RECTANGLE:
					missatge = testRectangle();
					break;
				case OPCIO_HEXAGON:
					missatge = testHexagon();
					break;
				case OPCIO_SORTIR:
					sortir = true;
					missatge = "Fins aviat!";
					break;
				default:
					missatge = "Opcio no valida";
					break;
			}
			System.out.println(missatge);
			System.out.println();
		} while(!sortir);
	}
	
	private String testCercle(){
		double radi = readDouble("Introdueix el radi del cercle:");
		Cercle figura = new Cercle (radi);
		String missatge = figura + " té area " + figura.calculaArea() + " i perímetre " + figura.calculaPerimetre();
		return missatge;
	}
	
	private String testQuadrat(){
		double costat = readDouble("Introdueix el costat del quadrat:");
		Quadrat figura = new Quadrat (costat);
		String missatge = figura + " té area " + figura.calculaArea() + " i perímetre " + figura.calculaPerimetre();
		return missatge;
	}
	
	private String testRectangle(){
		double base = readDouble("Introdueix la base del rectangle:");
		double altura = readDouble("Introdueix l'altura del rectangle:");
		Rectangle figura = new Rectangle (base, altura);
		String missatge = figura + " té area " + figura.calculaArea() + " i perímetre " + figura.calculaPerimetre();
		return missatge;
	}
	
	private String testHexagon(){
		double costat = readDouble("Introdueix el costat de l'hexàgon:");
		Hexagon figura = new Hexagon (costat);
		String missatge = figura + " té area " + figura.calculaArea() + " i perímetre " + figura.calculaPerimetre();
		return missatge;
	}
	
	/**
	 * Mètode auxiliar per a llegir un double fent servir l'objecte Scanner
	 * 
	 * @param message Missatge a mostrar a l'usuari
	 * @return valor llegit
	 */
	private double readDouble(String message) {
		System.out.println(message);
		double valor = input.nextDouble();
		input.nextLine(); //si no llegeixo el salt de linia, pot donar problemes desprès
		return valor;
	}
}
