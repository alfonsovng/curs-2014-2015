package exercici3.versio1;

public class CercleTest {

	public static void main(String[] args) {

		Cercle cercle1 = new Cercle();
		mostraInformacio(cercle1);
		
		Cercle cercle2 = new Cercle(33);
		mostraInformacio(cercle2);
		
		Cercle cercle3 = new Cercle();
		cercle3.setRadi(22.22);
		mostraInformacio(cercle3);
		
		mostraInformacio(new Cercle(7));
	}
	
	//enlloc de copiar això 4 vegades, ho he convertit en un mètode auxiliar
	private static void mostraInformacio(Cercle c) {
		System.out.println("El cercle de radi " + c.getRadi() 
				+ " té perímetre " + c.calculaPerimetre() 
				+ " i àrea " + c.calculaArea());
	}
}
