package exemple2;

/**
 * @see http://www.javatpoint.com/understanding-toString()-method
 */
public class ToStringTest {
	
	public static void main(String[] args) {
		
		Empleat empleat = new Empleat("Fernando", 24, 2430.5);
		/*
		 * Mostrarà per pantalla el valor 
		 * que retorni mètode toString() 
		 * de la classe Empleat 
		 */
		System.out.println(empleat);
	}
}
