package exemple2;

public class Empleat {

	private String nom;
	private int edat;
	private double sou;
	
	public Empleat(String nom, int edat, double sou) {
		this.nom = nom;
		this.edat = edat;
		this.sou = sou;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public double getSou() {
		return sou;
	}

	public void setSou(double sou) {
		this.sou = sou;
	}

	@Override
	public String toString() {
		return "Empleat [nom=" + nom + ", edat=" + edat + ", sou=" + sou + "]";
	}
}
