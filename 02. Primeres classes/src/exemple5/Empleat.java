package exemple5;

public class Empleat {

	private String nom;
	
	public Empleat() {
	}

	public Empleat(String nom) {
		this.nom = nom;
	}
	
	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	@Override
	public String toString() {
		return "Empleat [nom=" + nom + "]";
	}
}
