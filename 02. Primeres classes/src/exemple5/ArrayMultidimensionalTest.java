package exemple5;

public class ArrayMultidimensionalTest {

public static void main(String[] args) {
		
		int[][] array1 = new int[10][5];
		for(int i=0;i<array1.length;++i) {
			for(int j=0;j<array1[i].length;++j) {
				array1[i][j] = i*j;
			}
		}
		
		int[][] array2 = new int[][] { 
				{10, 20, 30},
				{40, 50, 60},
				{70, 80, 90}
		};
		
		int[][] array3 = new int[][] {
				{-1, -2, -3, -4, -5},
				{-6, -7, -8, -9},
				{-10, -11, -12},
				{-13, -14},
				{-15}
		};
		
		print("array1:",array1);
		print("array2:",array2);
		print("array3:",array3);
	}
	
	private static void print(String missatge, int[][] array) {
		System.out.println(missatge);
		for(int[] row: array) {
			for(int element: row) {
				System.out.print(element + "\t");
			}
			System.out.println();
		}
	}
}
