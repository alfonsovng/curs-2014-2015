package exemple5;

public class ArrayEntersTest {

	public static void main(String[] args) {

//		int[] c = new int[12];
//		int c[] = new int[12];
		int[] c;
		c = new int[12];
		
		System.out.println("El 1r element de l'array és " + c[0]);
		System.out.println("El darrer és " + c[11]);
		
//		int a = 3;
//		c[a+1]++;
		
		System.out.println("La longitud de l'array és " + c.length);
		
		for(int i=0;i<c.length;++i) {
			System.out.println("c[" + i + "]=" + c[i]);
		}
		
		int[] d = {10, 20, 54, -2, 76 };
		System.out.println("La longitud de l'array d és " + d.length);
		
		
		//declaració i creació de l'array
		int[] llista = new int[10];
		//modificació el valor dels elements de l'array
		for(int i=0;i<llista.length;++i) {
			llista[i] = i*2;
		}
		//pinta els elements de l'array
		for(int i=0;i<llista.length;++i) {
			System.out.print(llista[i] + " ");
		}
	}

}
