package exemple3;

public class MaximTest1 {

	public static void main(String[] args) {
		int numero1 = 20;
		int numero2 = 10;
		
		int numeroMaxim = maxim(numero1, numero2);
		
		System.out.println("El màxim de " + numero1 
				+ " i " + numero2 + " és " + numeroMaxim);
	}
	
	public static int maxim(int a, int b) {
		if(a > b) {
			return a;
		}
		return b;
	}
}
