package exemple3;

public class MaximTest2 {

	public static void main(String[] args) {
		int numero1 = 20;
		int numero2 = 10;
		
		int numeroMaxim = Math.max(numero1, numero2);
		
		System.out.println("El màxim de " + numero1 
				+ " i " + numero2 + " és " + numeroMaxim);
	}
}
