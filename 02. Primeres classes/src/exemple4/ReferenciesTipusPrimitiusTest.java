package exemple4;

public class ReferenciesTipusPrimitiusTest {

	public static void main(String[] args) {
		metodeInicial();
	}
	
	private static void metodeInicial() {
		int x = 60;
		
		System.out.println("'x' abans del metodeDeExemple: " + x);
		metodeDeExemple(x);
		System.out.println("'x' després del metodeDeExemple: " + x);
	}

	private static void metodeDeExemple(int x) {
		System.out.println("'x' dins del metodeDeExemple " +
				"abans de modificar-lo: " + x);
		x = 50;
		System.out.println("'x' dins del metodeDeExemple " +
				"després de modificar-lo: " + x);
	}
}
