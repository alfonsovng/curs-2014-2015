package exemple4;

public class ReferenciesObjectesTest {

	public static void main(String[] args) {
		metodeInicial();
	}
	
	private static void metodeInicial() {
		Alumne a = new Alumne();
		a.setNom("Wenceslao");
		a.setEdat(19);
		
		System.out.println("'a' abans del metodeDeExemple: " + a);
		metodeDeExemple(a);
		System.out.println("'a' després del metodeDeExemple: " + a);
	}

	private static void metodeDeExemple(Alumne a) {
		System.out.println("'a' dins del metodeDeExemple " +
				"abans de modificar-lo: " + a);
		
		//comentant la línia inferior pases a modificar l'objecte original
		a = new Alumne();
		a.setNom("Honorato");
		a.setEdat(44);
		
		System.out.println("'a' dins del metodeDeExemple " +
				"després de modificar-lo: " + a);
	}
}
