package exemple4;

public class Alumne {
	
	private int edat;
	private String nom;
	private String dni;
	


	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getEdat() {
		return edat;
	}

	public void setEdat(int edat) {
		this.edat = edat;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String toString() {
		return "Alumne [edat=" + edat + ", nom=" + nom + "]";
	}
}
