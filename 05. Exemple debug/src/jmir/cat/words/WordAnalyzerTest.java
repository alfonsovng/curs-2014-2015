package jmir.cat.words;

/**
 * Main class to test the {@link WordAnalyzer} class. 
 * 
 * @author <a href="mailto:alfonsodasilva@gmail.com">Alfonso da Silva</a>
 * @see <a href="http://www.horstmann.com/bigj/help/debugger/tutorial.html">http://www.horstmann.com/bigj/help/debugger/tutorial.html</a>
 * @version 1.0
 */
public class WordAnalyzerTest {

	/**
	 * Main method that uses <code>testFirstRepeatedCharacter</code>, 
	 * <code>testFirstMultipleCharacter</code> and <code>testFirstMultipleCharacter</code>
	 * methods
	 * 
	 * @param args ignored
	 * @see WordAnalyzerTest#testFirstMultipleCharacter(String)
	 * @see WordAnalyzerTest#testFirstMultipleCharacter(String)
	 * @see WordAnalyzerTest#testCountRepeatedCharacters(String)
	 */
	public static void main(String[] args) {
		testFirstRepeatedCharacter("aardvark"); // expect: a
		testFirstRepeatedCharacter("roommate"); // expect: o (not m)
		testFirstRepeatedCharacter("mate"); // expect: 0 (no duplicate letters)
		testFirstRepeatedCharacter("test"); // expect: 0 (the t isn't repeating)

		testFirstMultipleCharacter("missisippi"); // expect: i (not m or s)
		testFirstMultipleCharacter("mate"); // expect: 0 (no duplicate letters)
		testFirstMultipleCharacter("test"); // expect: t

		testCountRepeatedCharacters("mississippiii"); // expect: 4 (ss, ss, pp, iii)
		testCountRepeatedCharacters("test"); // expect: 0 (no repeated letters)
		testCountRepeatedCharacters("aabbcdaaaabb"); // expect: 4 (aa, bb, aaaa, bb)
	}

	/**
	 * Test the method firstRepeatedCharacter of the class WordAnalyzer
	 * 
	 * @param s word to analyze
	 * @see WordAnalyzer#firstRepeatedCharacter()
	 */
	public static void testFirstRepeatedCharacter(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		char result = wa.firstRepeatedCharacter();
		if (result == 0)
			System.out.println("No repeated character.");
		else
			System.out.println("First repeated character = " + result);
	}

	/**
	 * Test the method firstMultipleCharacter of the class WordAnalyzer
	 * 
	 * @param s word to analyze
	 * @see WordAnalyzer#firstMultipleCharacter()
	 */
	public static void testFirstMultipleCharacter(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		char result = wa.firstMultipleCharacter();
		if (result == 0)
			System.out.println("No multiple character.");
		else
			System.out.println("First multiple character = " + result);
	}

	/**
	 * Test the method countRepeatedCharacters of the class WordAnalyzer
	 * 
	 * @param s word to analyze
	 * @see WordAnalyzer#countRepeatedCharacters()
	 */
	public static void testCountRepeatedCharacters(String s) {
		WordAnalyzer wa = new WordAnalyzer(s);
		int result = wa.countRepeatedCharacters();
		System.out.println(result + " repeated characters.");
	}
}