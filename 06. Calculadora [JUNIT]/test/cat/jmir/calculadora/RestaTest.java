package cat.jmir.calculadora;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class RestaTest {

	Calculadora calculadora;
	
	@Before
	public void setUp() throws Exception {
		calculadora = new Calculadora();
	}

	@Test
	public void testRestaEnters() {
		String resultat = calculadora.calcula("45-10");
		assertEquals("35", resultat);
	}
	
	@Test
	public void testRestaDecimals() {
		String resultat = calculadora.calcula("4.5 - 1.3");
		assertEquals("3.2", resultat);
	}

}
