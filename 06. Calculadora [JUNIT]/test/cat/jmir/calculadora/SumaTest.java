package cat.jmir.calculadora;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SumaTest {

	Calculadora calculadora;
	
	@Before
	public void setUp() throws Exception {
		calculadora = new Calculadora();
	}

	@Test
	public void testSumaEnters() {
		String resultat = calculadora.calcula("7+11");
		assertEquals("18", resultat);
	}

	@Test
	public void testSumaDecimals() {
		String resultat = calculadora.calcula("0.5 + 0.6");
		assertEquals("1.1", resultat);
	}
	
	@Test
	public void testSumaNegatius() {
		String resultat = calculadora.calcula("-9 + 9");
		assertEquals("0", resultat);
	}
}
