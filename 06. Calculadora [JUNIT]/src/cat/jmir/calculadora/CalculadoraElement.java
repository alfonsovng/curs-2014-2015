package cat.jmir.calculadora;

public interface CalculadoraElement {
	
	public CalculadoraElement getParent();

	public void setParent(CalculadoraElement parent);
	
	public abstract float getValue();
	
	public abstract void addCharacter(char ch);
	
	public abstract void setLeftChild(CalculadoraElement leftChild);
	
	public abstract void setRightChild(CalculadoraElement rightChild);
}
