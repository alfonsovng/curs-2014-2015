package cat.jmir.calculadora.elements;

public class Multiplicacio extends AbstractOperation {

	@Override
	public float getValue() {
		return leftChild.getValue() * rightChild.getValue();
	}
}
