package cat.jmir.calculadora.elements;

import cat.jmir.calculadora.CalculadoraElement;

public abstract class AbstractElement implements CalculadoraElement {

	private CalculadoraElement parent;

	@Override
	public CalculadoraElement getParent() {
		return parent;
	}

	@Override
	public void setParent(CalculadoraElement parent) {
		this.parent = parent;
	}
}
