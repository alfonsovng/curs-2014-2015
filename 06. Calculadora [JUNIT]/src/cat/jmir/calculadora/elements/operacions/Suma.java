package cat.jmir.calculadora.elements.operacions;


public class Suma extends AbstractOperation {

	@Override
	public float getValue() {
		return leftChild.getValue() + rightChild.getValue();
	}
}
