package cat.jmir.calculadora.elements.operacions;

import cat.jmir.calculadora.CalculadoraElement;
import cat.jmir.calculadora.elements.AbstractElement;

abstract class AbstractOperation extends AbstractElement {

	CalculadoraElement leftChild;
	CalculadoraElement rightChild;

	@Override
	public void setLeftChild(CalculadoraElement leftChild) {
		this.leftChild = leftChild;
		leftChild.setParent(this);
	}

	@Override
	public void setRightChild(CalculadoraElement rightChild) {
		this.rightChild = rightChild;
		rightChild.setParent(this);
	}

	@Override
	public void addCharacter(char ch) {
		throw new IllegalArgumentException("Wrong character " + ch);
	}
}
