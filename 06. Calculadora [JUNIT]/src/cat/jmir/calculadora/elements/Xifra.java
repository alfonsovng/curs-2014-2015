package cat.jmir.calculadora.elements;

import cat.jmir.calculadora.CalculadoraElement;

public class Xifra extends AbstractElement {

	private final StringBuilder x = new StringBuilder();

	@Override
	public void addCharacter(char ch) {
		if(!Character.isWhitespace(ch)) {
			x.append(ch);
		}
	}

	@Override
	public float getValue() {
		if(x.length()>0) {
			try {
				return Float.parseFloat(x.toString());
			} catch (NumberFormatException e) {
				throw new IllegalArgumentException("Invalid number " + x);
			}
		} else {
			return 0;
		}
	}

	@Override
	public void setLeftChild(CalculadoraElement leftChild) {
		throw new IllegalArgumentException("Wrong token " + x);
	}

	@Override
	public void setRightChild(CalculadoraElement rightChild) {
		throw new IllegalArgumentException("Wrong token " + x);
	}
}
