package cat.jmir.calculadora;

import cat.jmir.calculadora.elements.Xifra;
import cat.jmir.calculadora.elements.operacions.Divisio;
import cat.jmir.calculadora.elements.operacions.Multiplicacio;
import cat.jmir.calculadora.elements.operacions.Resta;
import cat.jmir.calculadora.elements.operacions.Suma;

public class Calculadora {

	private static final char SUMA = '+';
	private static final char MULTIPLICACIO = '*';
	private static final char DIVISIO = '/';
	private static final char RESTA = '-';
		
	public Calculadora() {
	}
	
	public String calcula(String formula) {
		char[] chars = formula.toCharArray();
		CalculadoraElement current = new Xifra();
		for(int i=0;i<chars.length;++i) {
			current = check(current, chars[i]);
		}
		
		//busco el pare
		while (current.getParent() != null) {
			current = current.getParent();
		}
		
		float result = current.getValue();
		
		if(Math.round(result) == result) {
			//Es un enter!
			return String.valueOf((int) result);
		} else {
			return String.valueOf(result);
		}
	}
	
	private CalculadoraElement check(CalculadoraElement current, char ch) {
		switch(ch) {
		case SUMA: 
			return addOperation(new Suma(),current);
		case MULTIPLICACIO:
			return addOperation(new Multiplicacio(),current);
		case DIVISIO: 
			return addOperation(new Divisio(),current);
		case RESTA:
			return addOperation(new Resta(),current);
		default:
			current.addCharacter(ch);
			return current;
		}
	}
	
	private CalculadoraElement addOperation(CalculadoraElement operation, CalculadoraElement leftChild) {
		CalculadoraElement rightChild = new Xifra();
		operation.setLeftChild(leftChild);
		operation.setRightChild(rightChild);
		return rightChild;
	}
}
