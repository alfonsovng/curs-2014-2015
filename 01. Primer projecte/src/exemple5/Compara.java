package exemple5;

import java.util.Scanner;

/**
 * Programa d'exemple d'us dels diferents operadors
 * relacionals i de les sentències if i if...else
 * 
 */
public class Compara {

	public static void main(String[] args) {
		// Crea l'objecte Scanner
		Scanner input = new Scanner(System.in);

		//Defineixo les variables que faré servir
		int number1;
		int number2;

		// Li demana a l'usuari que entri un numero
		System.out.print("Introdueix el primer número: ");

		//Es llegeix el 1r numero
		number1 = input.nextInt() ;

		// Li demana a l'usuari que entri un numero
		System.out.print("Introdueix el segon número: ");

		//Es llegeix el 2n numero
		number2 = input.nextInt() ;

		//Es fan una sèrie de comparacions
		if ( number1 == number2 )
			System.out.println( "Els dos números són iguals");

		if ( number1 != number2 )
			System.out.println( "Els dos números són diferents");

		if ( number1 < number2 )
			System.out.println( "Els primer número és més petit que el segon");

		if ( number1 > number2 )
			System.out.println( "Els primer número és més gran que el segon");

		if ( number1 <= number2 )
			System.out.println( "Els primer número és més petit o igual que el segon");

		if ( number1 >= number2 )
			System.out.println( "Els primer número és més gran o igual que el segon");
	}
}
