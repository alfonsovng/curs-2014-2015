package exemple6;

import java.util.Scanner;

public class NotaMitja {

	public static void main(String[] args) {

		//llegire les notes del teclat
		Scanner input = new Scanner(System.in);

		int sumaTotalNotes;
		int contador;
		int notaActual;
		int notaMitja;

		//inicialitzo variables
		sumaTotalNotes = 0;
		contador = 1;

		//repeteixo 10 vegades
		while (contador <= 10) {

			System.out.println("Introduexi la nota " + contador + ": ");

			//llegeixo la nota
			notaActual = input.nextInt();

			//la acumulo
			sumaTotalNotes = sumaTotalNotes + notaActual;

			//actualitzo el contador
			contador = contador + 1;
		}

		//calculo la mitja (en forma d'enter.. si hi ha decimals es trunca)
		notaMitja = sumaTotalNotes / 10;

		System.out.println("La nota mitja és " + notaMitja);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}

