package exemple4;

public class ErrorSumaDouble {

	/**
	 * Exemple de la falta de precissió de les 
	 * operacions amb nombres de coma flotant
	 */
	public static void main(String[] args) {
		double a = 1.00;
		double b = 0.10;
		int c = 7;
		double resultat = (a-c*b);
		System.out.println("El resultat de " + a + "-" + c + "*" 
				+ b + " és " + resultat);
	}
}
