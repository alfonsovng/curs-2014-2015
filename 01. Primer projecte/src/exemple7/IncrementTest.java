package exemple7;

public class IncrementTest {
	
	public static void main(String[] args) {
		
		int i = 2;
		System.out.println(i++);
		System.out.println(i);
		
		int j = 2;
		System.out.println(++j); 
		System.out.println(j);
	}
}

