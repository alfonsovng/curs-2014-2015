package exemple1;
/* Això és el paquet (package) amb el que clasifico el codi. */

/**
 * Aquest és un programa d'exemple. Si mires el sistema de fitxers,
 * el fitxer que conté aquesta classe és diu Hola.java i es dins d'una
 * carpeta que es diu example (com el package).   
 */
public class HelloWorld {

	public static void main(String[] args) {
		//mostra per pantalla un text
		System.out.println("Hola a tothom");
		
		/*
		 * Aquest és un comentari de més d'una línia que poso com
		 * a exemple. L'asterisc que hi ha a l'esquerra l'ha posat
		 * l'eclipse, però no cal.
		 */
	}
}
