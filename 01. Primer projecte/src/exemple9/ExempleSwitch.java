package exemple9;

import java.util.Scanner;

public class ExempleSwitch {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		
		int numero = input.nextInt();
		
		switch(numero) {
			case 0: 
				System.out.println("Número zero");
				break;
			case 1:
				System.out.println("Número u");
				break;
			case 2:
				System.out.print("Número ");
				System.out.println("dos");
				break;
			case 3:
			case 4:
				System.out.println("Número tres o quatre");
				break;
			default:
				System.out.println("Número més gran que quatre o negatiu");
				break;
		}
	}
	
	
	
	
	
	
}
