package exemple9;

public class ExempleBreakContinue {

	public static void main(String[] args) {
		//exemple break
//		int a = 0;
//		
//		for( ; a<10; ++a) {
//			if (a == 5) break;
//			System.out.print(a + " ");
//		}
//
//		System.out.println();
//		System.out.println("S'ha sortit del bucle quan el comptador valia " + a);
		
		//exemple continue
		int b = 0;
		
		for( ; b<10; ++b) {
			if (b == 5) continue;
			System.out.print(b + " ");
		}

		System.out.println();
		System.out.println("S'ha sortit del bucle quan el comptador valia " + b);
	}

}
