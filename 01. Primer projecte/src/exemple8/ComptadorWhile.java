package exemple8;

public class ComptadorWhile {

	public static void main(String[] args) {
	
		//declaro i inicio la variable de control
		int comptador = 1;
		
		while (comptador <= 10) { //condició de continuitat del bucle
			
			System.out.print(comptador + " ");
			
			++comptador; //increment de la variable de control
		}
		
		System.out.println();
	}
}
