package exemple8;

public class ComptadorFor {

	public static void main(String[] args) {
		
		int comptador;
		
		//dins del for s'inclou la declaració i inicialització de
		//la variable de control, la condició de continuitat del bucle
		//i l'increment de la variable de control
		for(comptador=1 ; comptador <= 10; ++comptador) {
			System.out.print(comptador + " ");
		}
		
		System.out.print(comptador + " ");
		
		
		System.out.println();
	}

}
