package exemple2;

public enum Qualificacio {

	A("Molt bé!", true),
	B("Força bé", true),
	C("Bé", true),
	D("Justet", true),
	F("Malament", false);

	private final String message;
	private final boolean aprovat;
	
	Qualificacio(String message, boolean aprovat) {
		this.message = message;
		this.aprovat = aprovat;
	}
	
	public String getMessage() {
		return message;
	}

	public boolean isAprovat() {
		return aprovat;
	}
}