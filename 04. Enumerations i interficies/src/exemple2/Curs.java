package exemple2;

public class Curs {

	private int nota;
	
	private Qualificacio qualificacio;

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	
		switch(nota / 10) { //dona un valor entre 0 i 10
			case 10:
			case 9:
				qualificacio = Qualificacio.A;
				break;
			case 8:
				qualificacio = Qualificacio.B;
				break;
			case 7:
				qualificacio = Qualificacio.C;
				break;
			case 6:
				qualificacio = Qualificacio.D;
				break;
			default: //és 5 o més petit
				qualificacio = Qualificacio.F;
				break;
		}
	}
	
	public boolean isAprovat() {
		return qualificacio.isAprovat();
	}
	
	public String getMessage() {
		return qualificacio.getMessage();
	}
}
