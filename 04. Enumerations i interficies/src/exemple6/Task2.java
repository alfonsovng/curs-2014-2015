package exemple6;

import java.util.Random;

public class Task2 implements Runnable {
	
	private final Random random = new Random();
	
	public void run() {
		for(int i=0;i<10;++i) {
			System.out.println("** BUP! BUP! **");
			try {
				Thread.sleep(random.nextInt(2000));
			} catch (InterruptedException e) {
				System.err.println("Error: " + e.toString());
			}
		}
	}
}