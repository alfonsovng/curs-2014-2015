package exemple6;

public class TaskThreadTest {

	public static void main(String[] args) {
		
		Task1 task1 = new Task1();
		Thread thread1 = new Thread(task1);
		thread1.start();
		
		Task2 task2 = new Task2();
		Thread thread2 = new Thread(task2);
		thread2.start();
		
		Thread thread3 = new Thread(new Task3());
		thread3.start();

		
	}
}
