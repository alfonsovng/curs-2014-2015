package exemple6;

import java.util.Random;

public class Task3 implements Runnable {
	
	private final Random random = new Random();
	
	public void run() {
		for(int i=0;i<20;++i) {
			System.out.println("** MIAO! MIAO! **");
			try {
				Thread.sleep(random.nextInt(1000));
			} catch (InterruptedException e) {
				System.err.println("Error: " + e.toString());
			}
		}
	}
}