package exemple1;

public class Curs {

	private enum Qualificacio { A, B, C, D, F };

	private int nota;
	
	private Qualificacio qualificacio;

	public int getNota() {
		return nota;
	}

	public void setNota(int nota) {
		this.nota = nota;
	
		switch(nota / 10) { //dona un valor entre 0 i 10
			case 10:
			case 9:
				qualificacio = Qualificacio.A;
				break;
			case 8:
				qualificacio = Qualificacio.B;
				break;
			case 7:
				qualificacio = Qualificacio.C;
				break;
			case 6:
				qualificacio = Qualificacio.D;
				break;
			default: //és 5 o més petit
				qualificacio = Qualificacio.F;
				break;
		}
	}
	
	public boolean isAprovat() {
		return qualificacio != Qualificacio.F;
	}
	
	public String getMessage() {
		String message;
		switch(qualificacio) {
		case A:
			message = "Molt bé!";
			break;
		case B:
			message = "Força bé";
			break;
		case C:
			message = "Bé";
			break;
		case D:
			message = "Justet";
			break;
		case F:
			message = "Malament!";
			break;
		default:
			System.err.println("No contemplat");
			message = "???";
			break;
		}
		return message;
	}
}