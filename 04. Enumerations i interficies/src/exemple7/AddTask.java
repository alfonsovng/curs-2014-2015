package exemple7;

import java.util.List;

class AddTask implements Runnable {
 
    private int delay;
    private List<String> list;
    private int n;
     
    AddTask(List<String> list, int n, int delay) {
        this.list = list;
        this.n = n;
        this.delay = delay;
         
        //inicio la llista amb dos elements
        addToList(0);
        addToList(1);
    }
     
    private void addToList(int i) {
        list.add("element_" + i);
        System.out.println("S'ha afegit element " + i + ": " + list);
    }
    
    @Override
    public void run() {
        //el bucle comença per 2 pq els elements 0 i 1 s'afegeixen al constructor
        for(int i=2;i<n;++i) {
            addToList(i);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}