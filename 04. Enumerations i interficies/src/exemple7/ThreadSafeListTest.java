package exemple7;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;
 
/**
 * Exemple d'accés concurrent a llistes. Amb ArrayList es pot provocar
 * un error d'accés concurrent. En canvi, amb un Vector no es pot produir
 * per ser Thread safe.
 * 
 */
public class ThreadSafeListTest {
     
    //http://ezeon.in/blog/2014/02/thread-concurrency-test-on-arraylist-and-vector-object/
    public static void main(String[] args) {
 
        int delay=1;//thread delay
        int n=1000;//item count
//        List<String> list = new ArrayList<String>();//Async;not-thread-safe
        List<String> list = new Vector<String>();//Thread-safe
         
        new Thread(new AddTask(list, n, delay)).start();
        new Thread(new RemoveTask(list, n, delay)).start();
    }
}