package practica7;

import java.util.Scanner;

public class Menu {

	private Scanner input = new Scanner(System.in);
	private GestionaTasks gestor = new GestionaTasks();

	//-----------------------------------
	public void newMenu() {
		int opc;
		String misatge;
		int delay;
		int vegades;
		int pausa;
		boolean sortir = false;

		while (!sortir) {
			System.out.println("-----MENU-----");
			System.out.println("1.Afegir Tasca\n2.Arrancar tasques");
			opc = input.nextInt();
			switch (opc) {
			case 1:
				System.out
						.println("Introdueix el misatge que vols que es mostri:");
				misatge = input.next();
				do {
					System.out
							.println("Quant temps vols que tardi en arrancar(Delay)(Rang del 0 al 10)");
					delay = input.nextInt();
				} while (delay < 0 || delay > 10);
				do {
					System.out
							.println("Quantes vegades vols que es repeteixi?(Rang del 1 al 100)");
					vegades = input.nextInt();
				} while (vegades < 1 || vegades > 100);
				do {
					System.out
							.println("Segons entre repeticio?(-1 si vols que sigui random de 0-10)");
					pausa = input.nextInt();
				} while (pausa < -1 || pausa > 10);
				gestor.addTask(new Task(misatge, delay, vegades, pausa));
				break;
			case 2:
				gestor.start();
				sortir = true;
				break;
			default:
				System.out.println("Opció del menu no valida!");
				break;
			}
		}
	}

	public boolean comprobaAccions() {
		String accio = input.next();
		if (accio.equals("r")) {
			gestor.rapid();
		} else if (accio.equals("l")) {
			gestor.lent();
		} else if (accio.equals("s")) {
			gestor.stop();
			return false;
		}
		
		return true;
	}
}
