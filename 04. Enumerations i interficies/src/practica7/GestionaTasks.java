package practica7;

import java.util.ArrayList;

public class GestionaTasks {

	private final ArrayList<Task> tasks = new ArrayList<Task>();

	public void addTask(Task t) {
		tasks.add(t);
	}

	public void start() {
		for (int i = 0; i < tasks.size(); i++) {
			new Thread(tasks.get(i)).start();
		}
	}

	public void rapid() {
		System.out.println("Mes rapid...");
		for (int i = 0; i < tasks.size(); i++) {
			tasks.get(i).vesMesRapid();
		}
	}

	public void lent() {
		System.out.println("Mes lent...");
		for (int i = 0; i < tasks.size(); i++) {
			tasks.get(i).vesMesLent();
		}
	}

	public void stop() {
		System.out.println("Parant...");
		for (int i = 0; i < tasks.size(); i++) {
			tasks.get(i).stop();
		}
	}

}
