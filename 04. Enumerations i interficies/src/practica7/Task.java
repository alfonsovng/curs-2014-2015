package practica7;


public class Task implements Runnable, UserListener {

	private final String misatge;
	private final int vegades;
	private int delay;
	private int pausa;
	private boolean enabled;

	public Task(String misatge, int delayInSeconds, int vegades, int pausaInSeconds) {
		this.misatge = misatge;
		this.delay = delayInSeconds*1000;
		this.vegades = vegades;
		this.pausa = pausaInSeconds*1000;
		
		this.enabled = true;
	}

	public void run() {
		try {
			Thread.sleep(delay);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		for (int i = 0; i < vegades; i++) {
			System.out.println(misatge);
			try {
				Thread.sleep(pausa);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			if(!enabled) {
				break;
			}
		}
	}

	@Override
	public void vesMesRapid() {
		pausa*=0.9;
	}

	@Override
	public void vesMesLent() {
		pausa*=1.1;
	}

	@Override
	public void stop() {
		enabled = false;
	}
}
