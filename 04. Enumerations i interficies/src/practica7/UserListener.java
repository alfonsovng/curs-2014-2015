package practica7;

public interface UserListener {

	public void vesMesRapid();
	public void vesMesLent();
	public void stop();
}
