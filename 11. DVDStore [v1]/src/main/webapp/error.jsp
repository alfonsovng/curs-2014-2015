<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page language="java" pageEncoding="utf-8"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Alta i baixa de DVDs</title>
<link rel="stylesheet" href="css/dvd.css" />
<link rel="stylesheet" href="css/foundation.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
	
	<div id="myModal" class="reveal-modal full" data-reveal>
		<div class="row">
			<div class="large-2 columns">
				<a href="."><img src="img/error.gif" alt="error" /></a>
			</div>
			<div class="large-10 columns">
			  <h1>Error</h1>
			  <p class="lead">S'ha produït un error i tot el que se és això:</p> 
			  <p><span class="error">${error}</span></p>
			  <!-- Esborro la variable on es mostren els errors -->
			  <c:remove var="error"/>
			</div>
		</div>
		<div class="row">
			<div class="large-12 columns">
				<a class="button expand success" href=".">D'acord, no ho tornaré a fer</a>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<a class="button expand success" href=".">D'acord, no ho tornaré a fer</a>
		</div>
	</div>
	<!-- FOUNDATION JS -->
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
	<script>
		$(document).ready(
				function(){$('#myModal').foundation('reveal', 'open')}
		);
		$('#myModal').bind('closed', function() {
			 window.location.href = ".";
		});
	</script>
</body>
</html>