<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page language="java" pageEncoding="utf-8"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Alta i baixa de DVDs</title>
<link rel="stylesheet" href="css/dvd.css" />
<link rel="stylesheet" href="css/foundation.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="large-2 columns">
				<img src="img/dvd-logo.jpg" width="100" alt="DVD"/>
			</div>
			<div class="large-10 columns">
				<h1>Editar DVD</h1>
			</div>
			<hr />
			<div class="large-12 columns">
			<form id="updateForm" method="post" action=".">
				<div class="row">
					<div class="large-2 columns">
						<h4>Codi</h4>
					</div>
					<div class="large-2 columns">
						<h4>Tí­tol</h4>
					</div>
					<div class="large-2 columns">
						<h4>Autor</h4>
					</div>
					<div class="large-2 columns">
						<h4>Preu</h4>
					</div>
					<div class="large-2 columns">
						<h4>Etiquetes</h4>
					</div>					
					<div class="large-2 columns">
						<h4>Unitats</h4>
					</div>
				</div>
				<div class="row">
					<div class="large-2 columns">
						<input type="text" value="${disc.codi}" disabled />
						<input type="hidden" name="codi" value="${disc.codi}"/>
					</div>
					<div class="large-2 columns">
						<input type="text" name="titol" value="${disc.titol}" />
					</div>
					<div class="large-2 columns">
						<input type="text" name="autor"  value="${disc.autor}" />
					</div>
					<div class="large-2 columns">
						<div class="row collapse postfix-radius">
							<div class="small-8 columns">
								<input type="text" name="preu" value="${disc.preuBeauty}"/>
							</div>
							<div class="small-4 columns">
								<span class="postfix">&euro;</span>
							</div>
						</div>
					</div>
					<div class="large-2 columns">
						<input type="text" name="etiquetes"  value="${disc.etiquetesBeauty}" />
					</div>
					<div class="large-2 columns">
						<input type="text" name="unitats"  value="${disc.unitats}" />
					</div>
				</div>
				<div class="row">
					<div class="large-4 columns">
						<button class="button tiny expand success" name="action" value="save_update"
							type="submit">Guardar</button>
					</div>
					<div class="large-4 columns">
						<button class="button tiny alert expand" name="action" value="del"
							type="submit">Esborrar</button>
					</div>
					<div class="large-4 columns">
						<a href="." class="button tiny warning expand"
							type="submit">Cancelar</a>
					</div>
				</div>
			</form>
			</div>
		</div>
	</div>
	<!-- Esborro la variable on es mostren els errors -->
	<c:remove var="error"/>

	<!-- FOUNDATION JS -->
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
	<!-- JQUERY VALIDATION -->
	<script src="js/jquery.validate.min.js"></script>
	<script>
	$(document).ready(function() {
		$('.error').delay(5000).fadeOut('slow');
	    
		$.validator.addMethod('preuCorrecte', function (value, element, param) {
		    return /^[1-9]\d+(\.\d\d?)?$/.test(value);
		}, 'Preu incorrecte');
		
		$("#updateForm").validate({
			rules: {
				titol: "required",
				autor: "required",
				preu : { 
					preuCorrecte : true 
				},
				unitats: {
					required: true,
					min: 0,
					number: true
				}
			},
			messages: {
				titol: "Introdueix un títol",
				autor: "Introdueix un autor",
				unitats: "Nombre d'unitats incorrecte"
			}, 
			errorPlacement: function(error, element) {
				 error.insertAfter( element).position({
	                  my:'right top',
	                  at:'right top',
	                  of:element          
	              });
	              error.fadeOut(5000);
	        }
		});
	});
	</script>
</body>
</html>
