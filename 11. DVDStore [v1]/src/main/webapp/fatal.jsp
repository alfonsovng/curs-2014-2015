<%@page language="java" pageEncoding="utf-8" isErrorPage="true"%>
<!doctype html>
<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<title>Alta i baixa de DVDs</title>
<link rel="stylesheet" href="css/dvd.css" />
<link rel="stylesheet" href="css/foundation.css" />
<script src="js/vendor/modernizr.js"></script>
</head>
<body>
	<div class="container">
		<div class="row">
			<div class="large-3 columns">
				<img src="img/fatal.gif" alt="error" />
			</div>
			<div class="large-9 columns">
				<h1>Error</h1>  
				<p><span class="error"><%= exception.getMessage() %></span></p>
				<p>L'error ha estat noficat i en breu hi possarem sol·lució</p>
			</div>
		</div>
	</div>
				
	
	<!-- FOUNDATION JS -->
	<script src="js/vendor/jquery.js"></script>
	<script src="js/foundation.min.js"></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>

