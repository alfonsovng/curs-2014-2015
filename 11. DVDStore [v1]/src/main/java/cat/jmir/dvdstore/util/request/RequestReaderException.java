package cat.jmir.dvdstore.util.request;


public class RequestReaderException extends Exception {

	private static final long serialVersionUID = 1L;

	public RequestReaderException(String param, String error) {
		super(param + ": " + error);
	}
}
