package cat.jmir.dvdstore.util.request;

import javax.servlet.http.HttpServletRequest;

import cat.jmir.dvdstore.model.Disc;

public class RequestReader {
	
	public static Disc readDisc(HttpServletRequest request) throws RequestReaderException {
		String titol = readNonEmptyStringParam("titol", request);
		String autor = readNonEmptyStringParam("autor", request);
		int unitats = readNonNegativeIntParam("unitats", request);
		int preuCentims = readPrizeParam("preu", request);
		String[] etiquetes = readNonEmptyStringArrayParam("etiquetes", request);
		
		Disc d = new Disc();
		d.setTitol(titol);
		d.setAutor(autor);
		d.setPreuEnCentims(preuCentims);
		d.setUnitats(unitats);
		d.setEtiquetes(etiquetes);
		return d;
	}
	
	public static Disc readDiscWithCodi(HttpServletRequest request) throws RequestReaderException {
		int codi = readDiscCodi(request);
		Disc disc = readDisc(request);
		disc.setCodi(codi);
		return disc;
	}
	
	public static int readDiscCodi(HttpServletRequest request) throws RequestReaderException {
		return readNonNegativeIntParam("codi", request);
	}
	
	public static int[] readDiscCodis(HttpServletRequest request) throws RequestReaderException {
		return readNonNegativeIntArrayParam("codi", request);
	}
	
	private static int readNonNegativeIntParam(String paramName, HttpServletRequest request) throws RequestReaderException {
		int intValue = 0;
		
		String value = request.getParameter(paramName);
		if(value == null) {
			throw new RequestReaderException(paramName, "No existeix");
		} else {
			value = value.trim(); 
			if(value.isEmpty()) {
				throw new RequestReaderException(paramName, "No pot ser buit");
			} else {
				try {
					intValue = Integer.parseInt(value);
					if(intValue < 0) {
						throw new RequestReaderException(paramName, "No pot ser negatiu");
					}
				} catch(NumberFormatException e) {
					throw new RequestReaderException(paramName, "Ha de ser un valor numèric");
				}
			}
		}
		return intValue;
	}
	
	private static int[] readNonNegativeIntArrayParam(String paramName, HttpServletRequest request) throws RequestReaderException {
		int[] intValues = null;
		
		String[] values = request.getParameterValues(paramName);
		if(values == null) {
			throw new RequestReaderException(paramName, "No existeix");
		} else {
			intValues = new int[values.length];
			for(int i=0;i<intValues.length;++i) {
				try {
					intValues[i] = Integer.parseInt(values[i]);
					if(intValues[i] < 0) {
						throw new RequestReaderException(paramName, "No pot ser negatiu");
					}
				} catch(NumberFormatException e) {
					throw new RequestReaderException(paramName, "Ha de ser un valor numèric");
				}
			}
		}
		return intValues;
	}
	
	private static int readPrizeParam(String paramName, HttpServletRequest request) throws RequestReaderException {
		int intValue = 0;
		
		String value = request.getParameter(paramName);
		if(value == null) {
			throw new RequestReaderException(paramName, "No existeix");
		} else {
			value = value.trim(); 
			if(value.isEmpty()) {
				throw new RequestReaderException(paramName, "No pot ser buit");
			} else {
				try {
					String[] parts = value.split("\\.");
					if(parts.length == 1) {
						//sols euros
						intValue = Integer.parseInt(parts[0]) * 100;
					} else if(parts.length == 2) {
						//euros i centims
						intValue = Integer.parseInt(parts[0]) * 100;
						if(parts[1].length() == 1) {
							intValue += Integer.parseInt(parts[1])*10;
						} else if(parts[1].length() == 2) {
							intValue += Integer.parseInt(parts[1]);
						} else if(parts[1].length() != 0) {
							throw new RequestReaderException(paramName, "Format incorrecte");
						}
					} else {
						throw new RequestReaderException(paramName, "Format incorrecte");
					}
					
					if(intValue < 0) {
						throw new RequestReaderException(paramName, "No pot ser negatiu");
					}
				} catch(NumberFormatException e) {
					throw new RequestReaderException(paramName, "Ha de ser un valor numèric");
				}
			}
		}
		return intValue;
	}
	
	private static String readNonEmptyStringParam(String paramName, HttpServletRequest request) throws RequestReaderException {
		String value = request.getParameter(paramName);
		if(value == null) {
			throw new RequestReaderException(paramName, "No existeix");
		} else {
			//trec espais en blanc
			value = value.trim(); 
			if(value.isEmpty()) {
				throw new RequestReaderException(paramName, "No pot ser buit");
			}
		}
		return value;
	}
	
	private static String[] readNonEmptyStringArrayParam(String paramName, HttpServletRequest request) throws RequestReaderException {
		String[] values = request.getParameterValues(paramName);
		if(values == null) {
			throw new RequestReaderException(paramName, "No existeix");
		}
		return values;
	}
}
