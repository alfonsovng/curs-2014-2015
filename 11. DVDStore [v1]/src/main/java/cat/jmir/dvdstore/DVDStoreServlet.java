package cat.jmir.dvdstore;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import cat.jmir.dvdstore.model.Disc;
import cat.jmir.dvdstore.service.dao.DAOException;
import cat.jmir.dvdstore.service.dao.DAOFactory;
import cat.jmir.dvdstore.service.dao.DiscDAO;
import cat.jmir.dvdstore.service.dao.sql.ConnectionProvider;
import cat.jmir.dvdstore.util.request.RequestReader;
import cat.jmir.dvdstore.util.request.RequestReaderException;

public class DVDStoreServlet extends HttpServlet {

	final static Logger logger = Logger.getLogger(ConnectionProvider.class);
	
	private static final long serialVersionUID = 1L;

	private DiscDAO discDAO = null;
	
	@Override
	public void init(ServletConfig config) throws ServletException {
		super.init(config);
		logger.info("Inicialització del servlet");
		discDAO = DAOFactory.createDiscDAO();
	}
	
	@Override
	public void destroy() {
		logger.info("Destrucció del servlet");
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action == null) action = "list"; //acció per defecte!
		String view = "/error.jsp";
		try {
			view = doGetAction(action, request);
		} catch (DAOException e) {
			logger.error("Excepció de DAO", e);
			throw new ServletException("Excepció de DAO", e);
		} catch (RequestReaderException e) {
			request.getSession().setAttribute("error", e.getMessage());
			view = "/error.jsp";
		}
		
		getServletConfig().getServletContext().getRequestDispatcher(view).forward(request,response);
	}
		
	private String doGetAction(String action, HttpServletRequest request) throws RequestReaderException, DAOException {	
		if(action.equalsIgnoreCase("edit")) {
			int codi = RequestReader.readDiscCodi(request);
			Disc disc = discDAO.loadDisc(codi);
			request.setAttribute("disc", disc);
			return "/update.jsp";
		} else { //if(action.equalsIgnoreCase("list")) {
			List<Disc> discs = discDAO.loadAllDisc();
			request.setAttribute("discs", discs);
			return "/index.jsp";
		}
	}

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		if(action == null) action = "none"; //acció per defecte!
		try {
			doPostAction(action, request);
		} catch (DAOException e) {
			logger.error("Excepció de DAO", e);
			throw new ServletException("Excepció de DAO", e);
		} catch (RequestReaderException e) {
			request.getSession().setAttribute("error", e.getMessage());
			getServletConfig().getServletContext().getRequestDispatcher("/error.jsp").forward(request,response);
			return;
		}
		
		//@see http://es.wikipedia.org/wiki/Post/Redirect/Get
		response.sendRedirect(".");
	}
	
	private void doPostAction(String action, HttpServletRequest request) throws RequestReaderException, DAOException {	
		if(action.equalsIgnoreCase("del")) {
			delDisc(request);
		} else if(action.equalsIgnoreCase("new")) {
			newDisc(request);
		} else if(action.equalsIgnoreCase("save_update")) {
			updateDisc(request);
		} else {
			logger.warn("Valor d'action desconegut: " + action);
		}
	}
	
	private void delDisc(HttpServletRequest request) throws RequestReaderException, DAOException {
		int[] codis = RequestReader.readDiscCodis(request);
		logger.info("action del: " + Arrays.toString(codis));
		if(codis.length > 0) {
			discDAO.removeDisc(codis);
		} else {
			logger.warn("s'ha provat d'esborrar 0 dvds");
		}
	}
	
	private void newDisc(HttpServletRequest request) throws RequestReaderException, DAOException {
		Disc d = RequestReader.readDisc(request);
		logger.info("action new:" + d);
		discDAO.storeDisc(d);
	}
	
	private void updateDisc(HttpServletRequest request) throws RequestReaderException, DAOException {
		Disc d = RequestReader.readDiscWithCodi(request);
		logger.info("action update:" + d);
		discDAO.updateDisc(d);
	}
}


