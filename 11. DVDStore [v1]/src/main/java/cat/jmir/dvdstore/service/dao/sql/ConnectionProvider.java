package cat.jmir.dvdstore.service.dao.sql;

import java.sql.Connection;
import java.sql.SQLException;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import org.apache.commons.dbcp.BasicDataSource;
import org.apache.log4j.Logger;

import cat.jmir.dvdstore.service.config.ConfigProvider;

public class ConnectionProvider {
	
	final static Logger logger = Logger.getLogger(ConnectionProvider.class);
	
	private static ConnectionProvider instance;
	
	public static ConnectionProvider getConnectionProvider() {
		if(instance == null) instance = new ConnectionProvider();
		return instance;
	}
	
	private final DataSource dataSource;
	
	private ConnectionProvider() {
		ConfigProvider conf = ConfigProvider.getInstance(); 
		
		DataSource contextDataSource = null;
		if(conf.getDbContextDao()!=null) {
			logger.info("Recuperant un DAO extern");
			try {
				Context ctx = new InitialContext();
				contextDataSource = (DataSource) ctx.lookup("java:/comp/env/" + conf.getDbContextDao());
			} catch (NamingException e) {
				logger.error("Error recuperant el DAO " + conf.getDbContextDao(), e);
			}
		}
		
		if(contextDataSource != null) {
			this.dataSource = contextDataSource;
		} else {
			logger.info("Configurant un DAO intern");
			
			BasicDataSource ds = new BasicDataSource();
			ds.setDriverClassName(conf.getDbDriverClass());
			ds.setUrl(conf.getDbUrl());
			ds.setUsername(conf.getDbUsername());
			ds.setPassword(conf.getDbPassword());
	      
	        this.dataSource = ds;
		}
    }
	
	public Connection getConnection() throws SQLException {
		Connection con = dataSource.getConnection();
		con.setAutoCommit(true); //per defecte són autocommit
		return con;
	}
}












