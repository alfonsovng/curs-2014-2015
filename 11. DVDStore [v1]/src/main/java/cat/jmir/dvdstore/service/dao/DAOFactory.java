package cat.jmir.dvdstore.service.dao;

import org.apache.log4j.Logger;

import cat.jmir.dvdstore.service.config.ConfigProvider;
import cat.jmir.dvdstore.service.dao.mem.MemDiscDAO;
import cat.jmir.dvdstore.service.dao.sql.SQLDiscDao;

public final class DAOFactory {

	final static Logger logger = Logger.getLogger(DAOFactory.class);
	
	public static DiscDAO createDiscDAO() {
		
		String daoType = ConfigProvider.getInstance().getDao();
		
		logger.info("Instanciant el dao " + daoType);
		
		if(daoType.equalsIgnoreCase("MEMORY")) {
			return new MemDiscDAO();
		} else if(daoType.equalsIgnoreCase("SQL")) { 
			return new SQLDiscDao();
		} else {
			//default és en memòria
			DiscDAO dao = new MemDiscDAO();
			logger.warn("Tipus de repository desconegut: " + daoType);
			logger.warn("Es farà servir el DAO per defecte: " + dao.getClass());
			return dao;
		}
	}
}