package cat.jmir.dvdstore.service.config;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public final class ConfigProvider {

	private static ConfigProvider instance = null;

	public static ConfigProvider getInstance() {
		if(instance==null) instance = new ConfigProvider();
		return instance;
	}
	
	private final static Logger logger = Logger.getLogger(ConfigProvider.class);
	private final static String CONFIG_FILE = "dvd.properties";

	private final String dao;
	private final String dbContextDao;
	private final String dbDriverClass;
	private final String dbUrl;
	private final String dbUsername;
	private final String dbPassword;
	
	private ConfigProvider() {
		InputStream file = getClass().getClassLoader().getResourceAsStream(CONFIG_FILE);
		
		logger.info("Carregant les propietarts de " + CONFIG_FILE);
		
		Properties props = new Properties();
		try {
			props.load(file);
		} catch (IOException e) {
			logger.error("Error llegint les propietats de " + CONFIG_FILE, e);
			logger.warn("Es carregaran les propietats per defecte");
		}
		
		this.dao = props.getProperty("DAO", "MEMORY");
		this.dbContextDao = props.getProperty("DB_CONTEX_DAO", null);
		this.dbDriverClass = props.getProperty("DB_DRIVER_CLASS", "not necessary");
		this.dbUrl = props.getProperty("DB_URL", "not necessary");
		this.dbUsername = props.getProperty("DB_USERNAME", "not necessary");
		this.dbPassword = props.getProperty("DB_PASSWORD", "not necessary");
	}

	public String getDao() {
		return dao;
	}

	public String getDbContextDao() {
		return dbContextDao;
	}
	
	public String getDbDriverClass() {
		return dbDriverClass;
	}

	public String getDbUrl() {
		return dbUrl;
	}

	public String getDbUsername() {
		return dbUsername;
	}

	public String getDbPassword() {
		return dbPassword;
	}
}
