package cat.jmir.dvdstore.service.dao.mem;

import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import cat.jmir.dvdstore.model.Disc;
import cat.jmir.dvdstore.service.dao.DAOException;
import cat.jmir.dvdstore.service.dao.DiscDAO;

public class MemDiscDAO implements DiscDAO {

	final static Logger logger = Logger.getLogger(MemDiscDAO.class);
	
	private final List<Disc> discList = new ArrayList<Disc>();
	
	@Override
	public Disc loadDisc(int codi) throws DAOException {
		if(codi >= 0 && codi < discList.size()) {
			logger.info("loadDisc de codi " + codi);
			Disc d = discList.get(codi);
			if(d == null) {
				//és un disc que ha estat esborrat!!!
				throw new DAOException("No existeix el disc de codi " + codi);
			}
			return d;
		} else {
			throw new DAOException("No existeix el disc de codi " + codi);
		}
	}

	@Override
	public List<Disc> loadAllDisc() throws DAOException {
		/*
		 * He de tornar ua llista sense els nulls, és a dir, sense els esborrats
		 */
		List<Disc> discListCopy = new ArrayList<Disc>();
		for(Disc d: discList) {
			if(d!=null) discListCopy.add(d);
		}
		return discListCopy;
	}
	
	@Override
	public void storeDisc(Disc d) throws DAOException {
		/*
		 * el disc s'afegeix al final, per tant, el codi
		 * que tindrà és el de la mida actual de la llista 
		 */
		int codi = discList.size();
		/*
		 *  es posa el codi a l'objecte, per a mantenir
		 *  integres les dades
		 */
		d.setCodi(codi);
		discList.add(d);
		logger.info("storeDisc de codi " + codi);
	}

	@Override
	public void updateDisc(Disc d) throws DAOException {
		if(d.getCodi() >= 0 && d.getCodi() < discList.size()) {
			logger.info("updateDisc de codi " + d.getCodi());
			discList.set(d.getCodi(), d);
		} else {
			throw new DAOException("No existeix el disc de codi " + d.getCodi());
		}
	}
	
	@Override
	public void removeDisc(int... codis) throws DAOException {
		for (int codi : codis) {
			if(codi >= 0 && codi < discList.size()) {
				/*
				 * Poso un null al seu lloc per indicar que s'ha esborrat.
				 */
				discList.set(codi, null);
			} else {
				throw new DAOException("No existeix el disc de codi " + codi);
			}
		}
	}
}
