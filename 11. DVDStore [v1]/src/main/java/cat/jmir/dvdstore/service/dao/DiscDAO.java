package cat.jmir.dvdstore.service.dao;

import java.util.List;

import cat.jmir.dvdstore.model.Disc;

public interface DiscDAO {

	public Disc loadDisc(int codi) throws DAOException;
	
	public List<Disc> loadAllDisc() throws DAOException;
	
	public void storeDisc(Disc d) throws DAOException;
	
	public void updateDisc(Disc d) throws DAOException;
	
	public void removeDisc(int... codis) throws DAOException;
}
