package cat.jmir.dvdstore.service.dao.sql;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import cat.jmir.dvdstore.model.Disc;
import cat.jmir.dvdstore.service.dao.DAOException;
import cat.jmir.dvdstore.service.dao.DiscDAO;

public class SQLDiscDao implements DiscDAO {
	
	final static Logger logger = Logger.getLogger(SQLDiscDao.class);

	@Override
	public Disc loadDisc(int codi) throws DAOException {
		logger.info("loadDisc de codi " + codi);
		
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionProvider.getConnectionProvider().getConnection();
			stmt = con.prepareStatement("select v.id as id, v.autor as autor,v.titol as titol,"
					+ "v.unitats as unitats, v.preu as preu,group_concat(e.nom) as etiquetes "
					+ "from videoteca as v left join etiquetes as e on e.videoteca_id=v.id "
					+ "where v.id=? group by v.id");
			stmt.setInt(1, codi);
			
			rs = stmt.executeQuery();
			if(rs.next()) {
				return readDiscFromResultSet(rs);
			} else {
				logger.error("No existeix el disc de codi " + codi);
				throw new DAOException("No existeix el disc de codi " + codi);
			}
		} catch (SQLException e) {
			logger.error("Error al storeDisc amb codi " + codi, e);
			throw new DAOException("Error al loadDisc amb codi " + codi, e);
		} finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}

	@Override
	public List<Disc> loadAllDisc() throws DAOException {
		logger.info("loadAllDisc");
		
		Connection con = null;
		PreparedStatement stmt = null;
		ResultSet rs = null;
		try {
			con = ConnectionProvider.getConnectionProvider().getConnection();
			stmt = con.prepareStatement("select v.id as id, v.autor as autor,v.titol as titol,"
					+ "v.unitats as unitats, v.preu as preu,group_concat(e.nom) as etiquetes "
					+ "from videoteca as v left join etiquetes as e on e.videoteca_id=v.id "
					+ "group by v.id");
			
			rs = stmt.executeQuery();
			
			ArrayList<Disc> discList = new ArrayList<Disc>();
			while(rs.next()) {
				discList.add(readDiscFromResultSet(rs));
			}
			return discList;
		} catch (SQLException e) {
			logger.error("Error al loadAllDisc", e);
			throw new DAOException("Error al loadAllDisc", e);
		} finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}	
	}
	
	private Disc readDiscFromResultSet(ResultSet rs) throws SQLException {
		Disc d = new Disc();
		d.setCodi(rs.getInt("id"));
		d.setAutor(rs.getString("autor"));
		d.setTitol(rs.getString("titol"));
		d.setUnitats(rs.getInt("unitats"));
		d.setPreuEnCentims(rs.getInt("preu"));
		String etiquetes = rs.getString("etiquetes");
		if(etiquetes != null) {
			d.setEtiquetes(etiquetes.split("\\,"));
		}
		return d;
	}

	@Override
	public void storeDisc(Disc d) throws DAOException {
		logger.info("storeDisc amb codi " + d.getCodi());
		
		Connection con = null;
		PreparedStatement stmt = null;
		PreparedStatement stmtEI = null;
		ResultSet rs = null;
		try {
			con = ConnectionProvider.getConnectionProvider().getConnection();
			con.setAutoCommit(false);
			stmt = con.prepareStatement("insert into videoteca (autor,titol,unitats,preu) values (?,?,?,?)"
					,Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, d.getAutor());
			stmt.setString(2, d.getTitol());
			stmt.setInt(3, d.getUnitats());
			stmt.setInt(4, d.getPreuEnCentims());
			stmt.executeUpdate();
			
			//legeixo la pk inserida
			int codi;
			rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				codi = rs.getInt(1);
			} else {
				throw new DAOException("No s'ha generat la clau primaria en fer l'insert");
			}
			
			//insereixo les etiquetes
			stmtEI = con.prepareStatement("insert into etiquetes (videoteca_id,nom) values (?,?)");
			for(String etiqueta:d.getEtiquetes()) {
				stmtEI.setInt(1, codi);
				stmtEI.setString(2, etiqueta);
				stmtEI.executeUpdate();
			}
			
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ignored) {
			}
			logger.error("Error al storeDisc amb codi " + d.getCodi(), e);
			throw new DAOException("Error al storeDisc amb codi " + d.getCodi(), e);
		} finally {
			try {
				if(rs != null) rs.close();
				if(stmt != null) stmt.close();
				if(stmtEI != null) stmtEI.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	@Override
	public void updateDisc(Disc d) throws DAOException {
		logger.info("updateDisc amb codi " + d.getCodi());
		
		Connection con = null;
		PreparedStatement stmt = null;
		PreparedStatement stmtED = null;
		PreparedStatement stmtEI = null;
		try {
			con = ConnectionProvider.getConnectionProvider().getConnection();
			con.setAutoCommit(false);
			stmt = con.prepareStatement("update videoteca set autor=?,titol=?,unitats=?,preu=? where id=?");
			stmt.setString(1, d.getAutor());
			stmt.setString(2, d.getTitol());
			stmt.setInt(3, d.getUnitats());
			stmt.setInt(4, d.getPreuEnCentims());
			stmt.setInt(5, d.getCodi());
			stmt.executeUpdate();
			
			//esborro etiquetes
			stmtED = con.prepareStatement("delete from etiquetes where videoteca_id = ?");
			stmtED.setInt(1, d.getCodi());
			stmtED.executeUpdate();
			
			//les insereixo de nou
			stmtEI = con.prepareStatement("insert into etiquetes (videoteca_id,nom) values (?,?)");
			for(String etiqueta:d.getEtiquetes()) {
				stmtEI.setInt(1, d.getCodi());
				stmtEI.setString(2, etiqueta);
				stmtEI.executeUpdate();
			}
			
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ignored) {
			}
			logger.error("Error al updateDisc amb codi " + d.getCodi(), e);
			throw new DAOException("Error al updateDisc amb codi " + d.getCodi(), e);
		} finally {
			try {
				if(stmt != null) stmt.close();
				if(stmtED != null) stmtED.close();
				if(stmtEI != null) stmtEI.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
		
	@Override
	public void removeDisc(int... codis) throws DAOException {
		Connection con = null;
		PreparedStatement stmtV = null;
		PreparedStatement stmtED = null;
		try {
			con = ConnectionProvider.getConnectionProvider().getConnection();
			con.setAutoCommit(false);
			stmtV = con.prepareStatement("delete from videoteca where id = ?");
			stmtED = con.prepareStatement("delete from etiquetes where videoteca_id = ?");
			for (int codi : codis) {
				logger.info("removeDisc: codi " + codi);
				stmtV.setInt(1, codi);
				stmtV.executeUpdate();

				//no caldria esborrar les etiquetes pq hi ha un delete cascade...
				stmtED.setInt(1, codi);
				stmtED.executeUpdate();
			}
			con.commit();
		} catch (SQLException e) {
			try {
				con.rollback();
			} catch (SQLException ignored) {
			}
			logger.error("Error al removeDisc", e);
			throw new DAOException("Error al removeDisc", e);
		} finally {
			try {
				if(stmtV != null) stmtV.close();
				if(stmtED != null) stmtED.close();
				if(con != null) con.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}
}

