package cat.jmir.dvdstore.model;

import java.text.DecimalFormat;

/**
 * JavaBean que conté l'informació d'un DVD 
 */
public class Disc {

	private int codi;
	private String titol;
	private String autor;
	private String[] etiquetes;
	private int unitats;
	private int preuEnCentims;
	
	public int getCodi() {
		return codi;
	}
	
	public void setCodi(int codi) {
		this.codi = codi;
	}
	
	public String getTitol() {
		return titol;
	}
	
	public void setTitol(String titol) {
		this.titol = titol;
	}

	public String getAutor() {
		return autor;
	}

	public void setAutor(String autor) {
		this.autor = autor;
	}

	public int getUnitats() {
		return unitats;
	}

	public void setUnitats(int unitats) {
		this.unitats = unitats;
	}
	
	public String[] getEtiquetes() {
		return etiquetes;
	}

	public void setEtiquetes(String[] etiquetes) {
		this.etiquetes = etiquetes;
	}
	
	public String getEtiquetesBeauty() {
		StringBuilder b = new StringBuilder();
		if(etiquetes != null && etiquetes.length > 0) {
			b.append(etiquetes[0]);
			for(int i=1;i<etiquetes.length;++i) {
				b.append(',').append(etiquetes[i]);
			}
		}
		return b.toString();
	}

	public int getPreuEnCentims() {
		return preuEnCentims;
	}
	
	public void setPreuEnCentims(int preuEnCentims) {
		this.preuEnCentims = preuEnCentims;
	}
	
	public String getPreuBeauty() {
		DecimalFormat df = new DecimalFormat("#.00");
		return df.format(getPreuEnCentims() / 100.0);
	}

	@Override
	public String toString() {
		return "Disc [codi=" + codi + ", titol=" + titol + ", autor=" + autor
				+ ", unitats=" + unitats + ", preuEnCentims=" + preuEnCentims
				+ "]";
	}
}
 