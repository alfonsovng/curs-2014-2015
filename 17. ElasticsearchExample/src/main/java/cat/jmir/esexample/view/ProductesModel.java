package cat.jmir.esexample.view;

import java.util.List;

import cat.jmir.esexample.model.ProducteES;

public class ProductesModel {

	private String search;
	private List<ProducteES> productes;

	public String getSearch() {
		return search;
	}

	public void setSearch(String search) {
		this.search = search;
	}
	
	public List<ProducteES> getProductes() {
		return productes;
	}

	public void setProductes(List<ProducteES> productes) {
		this.productes = productes;
	}
}
