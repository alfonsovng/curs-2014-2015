package cat.jmir.esexample.view;

import java.util.List;

import cat.jmir.esexample.model.Producte;

public class EditarModel {

	private String email;
	private List<Producte> productes;

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Producte> getProductes() {
		return productes;
	}

	public void setProductes(List<Producte> productes) {
		this.productes = productes;
	}
}
