package cat.jmir.esexample.service.dao;

import java.util.List;

import cat.jmir.esexample.model.Producte;

public interface ProducteDAO {

	public List<Producte> loadAllProductes(int usuariId) throws DAOException;
	
	public void storeProducte(int usuariId, Producte p) throws DAOException;

	public void updateProducte(Producte p) throws DAOException;

	public void removeProducte(int producteId) throws DAOException;
}