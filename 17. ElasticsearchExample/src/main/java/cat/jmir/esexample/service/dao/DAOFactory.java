package cat.jmir.esexample.service.dao;

import org.apache.log4j.Logger;

import cat.jmir.esexample.service.dao.hibernate.HibernateProducteDAO;
import cat.jmir.esexample.service.dao.hibernate.HibernateUsuariDAO;

public final class DAOFactory {

	final static Logger logger = Logger.getLogger(DAOFactory.class);
	
	public static ProducteDAO createProducteDAO() {
		logger.info("loading ProducteDAO...");
		return new HibernateProducteDAO();
	}
	
	public static UsuariDAO createUsuariDAO() {
		logger.info("loading UsuariDAO...");
		return new HibernateUsuariDAO();
	}
}