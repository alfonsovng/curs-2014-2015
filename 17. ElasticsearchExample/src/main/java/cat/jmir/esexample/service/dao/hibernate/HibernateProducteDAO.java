package cat.jmir.esexample.service.dao.hibernate;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.hibernate.Session;

import cat.jmir.esexample.model.Producte;
import cat.jmir.esexample.model.Usuari;
import cat.jmir.esexample.service.dao.DAOException;
import cat.jmir.esexample.service.dao.ProducteDAO;

public class HibernateProducteDAO implements ProducteDAO {

	@Override
	public List<Producte> loadAllProductes(int usuariId) throws DAOException {
		
		Session session = HibernateUtil.getSession();

		Usuari u = (Usuari) session.get(Usuari.class, usuariId);
		
		List<Producte> productes = new ArrayList<Producte>();
		productes.addAll(u.getProductes());

		//tanco la sessió
		session.close();

		return productes;
	}

	@Override
	public void storeProducte(int usuariId, Producte p) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void updateProducte(Producte p) throws DAOException {
		// TODO Auto-generated method stub

	}

	@Override
	public void removeProducte(int producteId) throws DAOException {
		// TODO Auto-generated method stub

	}

}
