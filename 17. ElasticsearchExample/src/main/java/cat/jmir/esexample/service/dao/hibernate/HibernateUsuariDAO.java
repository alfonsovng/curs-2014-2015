package cat.jmir.esexample.service.dao.hibernate;

import org.hibernate.Query;
import org.hibernate.Session;

import cat.jmir.esexample.model.Usuari;
import cat.jmir.esexample.service.dao.DAOException;
import cat.jmir.esexample.service.dao.UsuariDAO;

public class HibernateUsuariDAO implements UsuariDAO {

	/**
	 * Busca un usuari pel seu mail
	 * 
	 * @param mail
	 * @return null si no exiteix l'usuari
	 * @throws DAOException
	 */
	@Override
	public Usuari loadUsuari(String mail) throws DAOException {
		
		Session session = HibernateUtil.getSession();
		
		Query q = session.createQuery("from Usuari where mail=:mail");
		q.setString("mail", mail);
		
		Usuari u = (Usuari) q.uniqueResult();
		
		//tanco la sessió
		session.close();
		
		return u;
	}
}
