package cat.jmir.esexample.service.dao;

import cat.jmir.esexample.model.Usuari;

public interface UsuariDAO {

	/**
	 * Busca un usuari pel seu mail
	 * 
	 * @param mail
	 * @return null si no exiteix l'usuari
	 * @throws DAOException
	 */
	public Usuari loadUsuari(String mail) throws DAOException;
}
