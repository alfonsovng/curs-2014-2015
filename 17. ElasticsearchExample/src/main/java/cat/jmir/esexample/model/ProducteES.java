package cat.jmir.esexample.model;

public class ProducteES {

	private int id;
    
	private String descripcioCurta;
    
	private String descripcioLlarga;
    
    private double lng;
    
    private double lat;
    
    private String contacte;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcioCurta() {
		return descripcioCurta;
	}

	public void setDescripcioCurta(String descripcioCurta) {
		this.descripcioCurta = descripcioCurta;
	}

	public String getDescripcioLlarga() {
		return descripcioLlarga;
	}

	public void setDescripcioLlarga(String descripcioLlarga) {
		this.descripcioLlarga = descripcioLlarga;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public String getContacte() {
		return contacte;
	}

	public void setContacte(String contacte) {
		this.contacte = contacte;
	}
}
