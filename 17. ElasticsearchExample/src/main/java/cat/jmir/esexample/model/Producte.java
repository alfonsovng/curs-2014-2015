package cat.jmir.esexample.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name="producte")
public class Producte {

    @Id
    @GeneratedValue
    @Column(name="pk")
	private int id;
    
    @Column(nullable=false)
	private String descripcioCurta;
    
    @Column
	private String descripcioLlarga;
    
    @Column
    private double lng;
    
    @Column
    private double lat;
	
    @ManyToOne
    @JoinColumn(name="propietari_id")
    private Usuari propietari;
    
	public Producte() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDescripcioCurta() {
		return descripcioCurta;
	}

	public void setDescripcioCurta(String descripcioCurta) {
		this.descripcioCurta = descripcioCurta;
	}

	public String getDescripcioLlarga() {
		return descripcioLlarga;
	}

	public void setDescripcioLlarga(String descripcioLlarga) {
		this.descripcioLlarga = descripcioLlarga;
	}

	public double getLng() {
		return lng;
	}

	public void setLng(double lng) {
		this.lng = lng;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public Usuari getPropietari() {
		return propietari;
	}

	public void setPropietari(Usuari propietari) {
		this.propietari = propietari;
	}

	@Override
	public String toString() {
		return "Producte [id=" + id + ", descripcioCurta=" + descripcioCurta
				+ ", descripcioLlarga=" + descripcioLlarga + ", lng=" + lng
				+ ", lat=" + lat + ", propietari=" + propietari + "]";
	}
}
