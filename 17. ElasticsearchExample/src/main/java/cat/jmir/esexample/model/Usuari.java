package cat.jmir.esexample.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name="usuari")
public class Usuari {

	@Id
	@GeneratedValue
	@Column(name="pk")
	private int id;
	
	@Column(nullable=false)
	private String nom;

	@Column(nullable=false,unique=true)
	private String mail;
	
	@OneToMany(mappedBy="propietari")
	private Set<Producte> productes;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public Set<Producte> getProductes() {
		return productes;
	}

	public void setProductes(Set<Producte> productes) {
		this.productes = productes;
	}

	public String getMail() {
		return mail;
	}

	public void setMail(String mail) {
		this.mail = mail;
	}

	@Override
	public String toString() {
		return "Usuari [id=" + id + ", nom=" + nom + ", mail=" + mail + "]";
	}
}
