package cat.jmir.esexample;

import java.io.IOException;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.PreDestroy;
import javax.inject.Singleton;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.UriBuilder;

import org.apache.log4j.Logger;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.client.transport.TransportClient;
import org.elasticsearch.common.transport.InetSocketTransportAddress;
import org.elasticsearch.common.xcontent.XContentFactory;
import org.elasticsearch.index.query.MatchAllQueryBuilder;
import org.elasticsearch.index.query.QueryStringQueryBuilder;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.glassfish.jersey.server.mvc.Viewable;

import cat.jmir.esexample.model.Producte;
import cat.jmir.esexample.model.ProducteES;
import cat.jmir.esexample.model.Usuari;
import cat.jmir.esexample.service.dao.DAOException;
import cat.jmir.esexample.service.dao.DAOFactory;
import cat.jmir.esexample.service.dao.ProducteDAO;
import cat.jmir.esexample.service.dao.UsuariDAO;
import cat.jmir.esexample.view.EditarModel;
import cat.jmir.esexample.view.ProductesModel;

@Path("/")
@Singleton
public class Controller {
	
	private Logger log = Logger.getLogger(Controller.class);
	
	private final URI productesURI; //per redireccionar a productes
	private final URI editaURI; //per redireccionar a edita
	
	private final ProducteDAO producteDAO;
	private final UsuariDAO usuariDAO;
	private final Client esClient;
	
	public Controller() {
		log.info("Loading controller!");
		
		this.productesURI = UriBuilder.fromUri("productes").build();
		this.editaURI = UriBuilder.fromUri("edita").build();
		
		this.producteDAO = DAOFactory.createProducteDAO();
		log.info("Loaded producteDAO: " + producteDAO.getClass());
		
		this.usuariDAO = DAOFactory.createUsuariDAO();
		log.info("Loaded usuariDAO: " + usuariDAO.getClass());
		
		esClient = new TransportClient()
        	.addTransportAddress(new InetSocketTransportAddress("localhost", 9300));
	}
	
	@GET
	public Response index() {
		//redirecciona a productes
		return Response.seeOther(productesURI).build();
	}
	
	@GET
	@Path("/productes")
    public Response productes(@QueryParam("search") String search) {
		if(search == null) {
			search = "";
		} else {
			search = search.trim();
		}
		log.info("Searching " + search);
		
		//TODO fa una cerca al servidor elasticsearch i guarda el resultat dins del model
		SearchRequestBuilder s = new SearchRequestBuilder(esClient);
		s.setIndices("products");
		s.setTypes("product");
		
		if(search.isEmpty()) {
			s.setQuery(new MatchAllQueryBuilder());
		} else {
			s.setQuery(new QueryStringQueryBuilder(search));
		}
		
		SearchResponse resposta = s.execute().actionGet();

		SearchHits searchHits = resposta.getHits();
		
		//long numeroResultats = searchHits.getTotalHits();
		
		List<ProducteES> listaProductes = new ArrayList<ProducteES>();
		
		SearchHit[] hits = searchHits.getHits();
		for(int i=0;i<hits.length;++i) {
			SearchHit hit = hits[i];
			
			Map<String, Object> source = hit.getSource();

			int id = Integer.parseInt(hit.getId());
			String dC = (String) source.get("descripcioCurta");
			String dL = (String) source.get("descripcioLlarga");
			String contacte = (String) source.get("contacte");
			
			Map<String, Double> coor = (Map<String, Double>) source.get("coordenades");

			ProducteES product = new ProducteES();
			product.setId(id);
			product.setDescripcioCurta(dC);
			product.setDescripcioLlarga(dL);
			product.setContacte(contacte);
			product.setLng(coor.get("lon"));
			product.setLat(coor.get("lat"));
			
			listaProductes.add(product);
		}
		
		ProductesModel model = new ProductesModel();
		model.setSearch(search);
		model.setProductes(listaProductes);
        return Response.ok(new Viewable("/productes", model)).build();
    }
	
	@GET
	@Path("/edita")
	public Response edita(@Context HttpServletRequest req) throws DAOException {
		HttpSession session = req.getSession(true);
		Integer usuariId = (Integer) session.getAttribute("usuari_id");
    	if (usuariId==null) {
    		log.info("No user defined!");
    		return Response.status(Status.UNAUTHORIZED).build();
    	}
    	
    	List<Producte> list = producteDAO.loadAllProductes(usuariId);
   	
		
		EditarModel model = new EditarModel();
		model.setEmail((String) session.getAttribute("usuari_mail"));
		model.setProductes(list);
		
		return Response.ok(new Viewable("/edita", model)).build();
	}
	
	@POST
	@Path("/login")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response login(@Context HttpServletRequest req, @FormParam("email") String email) throws DAOException {
		log.info("Login from " + email);
		
		Usuari usuari = usuariDAO.loadUsuari(email);
			
		if(usuari == null) {
			log.info("No existeix l'usuari " + email);
			return Response.status(Status.UNAUTHORIZED).build();
		}
		
		//guardo l'usuari a la sessió
		HttpSession session = req.getSession(true);
		
		//guardo l'usuari a la sessió
		session.setAttribute("usuari_id", usuari.getId());
		
		//i el seu mail, per comoditat
		session.setAttribute("usuari_mail", usuari.getMail());
		
		//http://en.wikipedia.org/wiki/Post/Redirect/Get
        return Response.seeOther(editaURI).build();
    }
	
	@POST
	@Path("/nou")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response insereixProducte(
			@Context HttpServletRequest req,
			@FormParam("descCurta") String descCurta,
			@FormParam("descLlarga") String descLlarga,
			@FormParam("longitud") double longitud,
			@FormParam("latitud") double latitud
		) {
		log.info("insereixProducte: " + descCurta + "|" + descLlarga + "|" + longitud + "|" + latitud + "|");
		
		HttpSession session = req.getSession(true);
		Integer usuariId = (Integer) session.getAttribute("usuari_id");
    	if (usuariId==null) {
    		log.info("No user defined!");
    		return Response.status(Status.UNAUTHORIZED).build();
    	}
		
		//TODO fa un insert a la BBDD amb Hibernate
		
		//http://en.wikipedia.org/wiki/Post/Redirect/Get
        return Response.seeOther(editaURI).build();
    }
	
	@POST
	@Path("/salva/{id}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response actualitzaProducte(
			@PathParam("id") int id,
			@FormParam("descCurta") String descCurta,
			@FormParam("descLlarga") String descLlarga,
			@FormParam("longitud") double longitud,
			@FormParam("latitud") double latitud
		) {
		log.info("actualitzaProducte: " + id + "|" + descCurta + "|" + descLlarga + "|" + longitud + "|" + latitud + "|");
		
		//TODO fa un update a la BBDD amb Hibernate
		
		//http://en.wikipedia.org/wiki/Post/Redirect/Get
        return Response.seeOther(editaURI).build();
    }
	
	@POST
	@Path("/esborra/{id}")
	@Consumes(MediaType.APPLICATION_FORM_URLENCODED)
	public Response esborraProducte(@PathParam("id") int id) {
		
		log.info("Esborrant producte amb id: " +  id);
		
		deleteProducteElasticSearch(id);
		
		return Response.seeOther(editaURI).build();
	}
	
	@GET
	@Path("/logout")
	public Response logout(@Context HttpServletRequest req) {
		//esborro l'usuari a la sessió
		req.getSession(true).removeAttribute("usuari_id");
		
		//http://en.wikipedia.org/wiki/Post/Redirect/Get
		return Response.seeOther(productesURI).build();
	}
	
	/**
	 * Mètode per actualitzar un producte a ElasticSearch.
	 * Ho fa de manera asíncrona, és a dir, no bloquejant.
	 * @param p
	 */
	private void actualitzaProducteElasticSearch(Producte p) {
		try {
			Map<String, Double> coordenadesMap = new HashMap<String, Double>();
			coordenadesMap.put("lon", p.getLng());
			coordenadesMap.put("lat", p.getLat());
			
			esClient.prepareUpdate("products", "product", String.valueOf(p.getId()))
				.setDoc(XContentFactory.jsonBuilder().startObject()
					.field("descripcioCurta", p.getDescripcioCurta())
					.field("descripcioLlarga", p.getDescripcioLlarga())
					.field("contacte", p.getPropietari().getMail())
					.field("coordenades", coordenadesMap)
				.endObject()
			).execute();
		} catch (IOException e) {
			log.warn("Error a actualitzaProducteElasticSearch", e);
		}
	}
	
	/**
	 * Mètode per inserir un producte a ElasticSearch.
	 * Ho fa de manera asíncrona, és a dir, no bloquejant.
	 * @param p
	 */
	private void nouProducteElasticSearch(Producte p) {
		try {
			Map<String, Double> coordenadesMap = new HashMap<String, Double>();
			coordenadesMap.put("lon", p.getLng());
			coordenadesMap.put("lat", p.getLat());
			
			esClient.prepareIndex("products", "product", String.valueOf(p.getId()))
				.setSource(XContentFactory.jsonBuilder().startObject()
					.field("descripcioCurta", p.getDescripcioCurta())
					.field("descripcioLlarga", p.getDescripcioLlarga())
					.field("contacte", p.getPropietari().getMail())
					.field("coordenades", coordenadesMap)
				.endObject()
			).execute();
		} catch (IOException e) {
			log.warn("Error a nouProducteElasticSearch", e);
		}
	}
	
	/**
	 * Mètode per esborrar un producte a ElasticSearch.
	 * Ho fa de manera asíncrona, és a dir, no bloquejant.
	 * @param p
	 */
	private void deleteProducteElasticSearch(int producteId) {
		esClient.prepareDelete("products", "product", String.valueOf(producteId)).execute();
	}
	
	/**
	 * Afegint aquest mètode fas que abans de tancar-se o reiniciar-se
	 * el servidor, es tanquin els recursos "crítics"
	 */
	@PreDestroy
	public void closeController() {
		log.info("Closing resources...");
		esClient.close();
	}
}
