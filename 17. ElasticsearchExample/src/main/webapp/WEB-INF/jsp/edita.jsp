<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mr House</title>
<link rel="stylesheet" href='css/normalize.css' />
<link rel="stylesheet" href='css/foundation.css' />
<script src='js/vendor/modernizr.js'></script>
</head>
<body>
	<div class="panel">
		<div class="row">
			<div class="large-10 columns">
				<h2>Productes de ${it.email}</h2>
			</div>
			<div class="large-2 columns">
				<a class="button expand" href="logout">Sortir</a>
			</div>
		</div>
	</div>
	<c:forEach var="p" items="${it.productes}">
		<!-- init product-->
		<div class="row">
			<form method="post" action="salva/${p.id}">
				<div class="large-2 columns">
					<label>Descripció curta<input value="${p.descripcioCurta}" type="text" name="descCurta" placeholder="Descripció curta" required></label>
				</div>
				<div class="large-4 columns">
					<label>Descripció llarga<input value="${p.descripcioLlarga}" type="text" name="descLlarga" placeholder="Descripció llarga" required></label>
				</div>		
				<div class="large-2 columns">
					<label>Longitud<input value="${p.lng}" type="number" step="0.000001" name="longitud" placeholder="Longitud" required></label>
				</div>
				<div class="large-2 columns">
					<label>Latitud<input value="${p.lat}" type="number" step="0.000001" name="latitud" placeholder="Latitud" required></label>
				</div>
				<div class="large-1 columns">
					<input type="submit" title="Guardar" class="button success expand large" value="G"/>
				</div>
			</form>
			<form method="post" action="esborra/${p.id}">
				<div class="large-1 columns">
					<input type="submit" title="Esborrar" class="button alert expand large" value="X"/>
				</div>
			</form>
		</div>
		<!-- end product-->
	</c:forEach>
	
	<div class="row">
		<div class="large-12 columns">
			<hr />
		</div>
	</div>
	<div class="row">
		<form method="post" action="nou">
			<div class="large-2 columns">
				<label>Descripció curta<input type="text" name="descCurta" placeholder="Descripció curta" required></label>
			</div>
			<div class="large-4 columns">
				<label>Descripció llarga<input type="text" name="descLlarga" placeholder="Descripció llarga" required></label>
			</div>		
			<div class="large-2 columns">
				<label>Longitud<input type="number" step="0.000001" name="longitud" placeholder="Longitud" required></label>
			</div>
			<div class="large-2 columns">
				<label>Latitud<input type="number" step="0.000001" name="latitud" placeholder="Latitud" required></label>
			</div>
			<div class="large-2 columns">
				<input type="submit" title="Inserir nou producte" class="button success expand large" value="Nou"/>
			</div>
		</form>
	</div>
	<script src='js/vendor/jquery.js'></script>
	<script src='js/foundation.min.js'></script>
	<script src='js/map.js'></script>
	<script>
		$(document).foundation();
	</script>
</body>
</html>
