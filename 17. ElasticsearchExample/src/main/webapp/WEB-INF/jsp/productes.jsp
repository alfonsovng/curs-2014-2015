<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@page language="java" pageEncoding="utf-8"%>
<!DOCTYPE html>
<!--[if IE 9]><html class="lt-ie10" lang="en" ><![endif]-->
<html class="no-js" lang="en">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Mr House</title>
<link rel="stylesheet" href='css/normalize.css' />
<link rel="stylesheet" href='css/foundation.css' />
<link rel="stylesheet" href="css/ol.css" type="text/css">
<link rel="stylesheet" href="css/app.css" type="text/css">
<script src="js/ol.js" type="text/javascript"></script>
<script src='js/vendor/modernizr.js'></script>
</head>
<body>
	<div class="row" id="logo">
		<div class="large-12 columns">
			<div class="row">
				<div class="large-12 columns">
					<form action="productes" method="GET">
						<div class="row collapse">
							<div class="large-6 columns large-offset-3">
								<br /> <br /> <br />
								<input type="search" name="search" value="${it.search}" placeholder="Search in Mr House"></input>
								<br />
							</div>
							<div class="large-3 columns">
								<br /> <br /> <br />
								<button type="submit" class="button postfix">Vull trobar la meva nova llar!</button>
								<br />
							</div>
						</div>
					</form>
				</div>
			</div>
			<div class="row">
				<div class="large-7 columns">
					<div id="map" class="map"></div>
				</div>
				<div class="large-5 columns" id="products">
					<div class="row">
			            <div class="large-12 columns">
			                <dl class="sub-nav right">
			                	<dd><a href="#">&laquo;</a></dd>
			                	<dt>Mostrant 1 – 3 de 3</dt>
			                    <dd class="active"><a href="#">&raquo;</a></dd>
			                </dl>
			            </div>
			        </div>
			        <c:forEach var="p" items="${it.productes}">
					<!-- init product-->
					<div class="row" id="${p.id}">
						<div class="large-12 columns">
							<br />
							<div class="row">
								<div class="large-12 columns">
									<a href="#${p.id}" id="button_${p.id}" class="button expand info large">${p.descripcioCurta}</a>
								</div>
							</div>
							<div class="row">
								<div class="large-12 columns">
									<p>${p.descripcioLlarga}</p>
								</div>
							</div>
							<div class="row">
								<div class=" large-6 columns large-offset-6">
									<p class="panel callout radius text-center">
										<strong><a href="mailto:${p.contacte}">Contacte</a></strong>
									</p>
								</div>
							</div>
						</div>
					</div>
					<!-- end product-->
					</c:forEach>
				</div>
			</div>
		</div>
	</div>
	<footer class="row">
		<div class="large-12 columns">
			<hr />
			<div class="row">
				<div class="large-3 columns">
					<p>&#169; 2015 Mr House SA</p>
				</div>
				<div class="large-9 columns">
					<ul class="inline-list right">
						<li><a href="#" data-reveal-id="login-modal">LOGIN</a></li>
					</ul>
					<div id="login-modal" class="reveal-modal" data-reveal>
						<div class="row">
							<div
								class="small-12 medium-10 medium-centered large-8 large-centered columns">
								<form action="login" method="POST">
									<fieldset>
										<legend>Mr House Sign In</legend>
										<div class="row">
											<div class="large-12 columns">
												<input type="text" name="email" placeholder="Email">
											</div>
										</div>
										<div class="row">
											<div class="large-6 columns">
												<input type="submit" class="button success expand" value="Login"></input>
											</div>
											<div class="large-6 columns">
												<a href="#" id="close-login-modal" class="button secondary expand">Cancel</a>
											</div>
										</div>
									</fieldset>
								</form>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</footer>
	<script src='js/vendor/jquery.js'></script>
	<script src='js/foundation.min.js'></script>
	<script src='js/map.js'></script>
	<script>
		$(document).foundation();

		//Quan es carrega...
		$(document).ready(function() {
			resizeMapAndProducts();
			loadMap();
			setIcons();
		});

		//Quan es canvia la mida de la pantalla
		$(window).resize(function() {
			resizeMapAndProducts();
			map.updateSize();
		});

		function resizeMapAndProducts() {
			$("#map").css("height", (window.innerHeight - 240) + "px");
			$("#products").css("height", (window.innerHeight - 240) + "px");
		}

		function setIcons() {
			//poso icones per cada producte
			<c:forEach var="p" items="${it.productes}">
			setIconOnMap(${p.lng}, ${p.lat}, "#${p.id}", "#button_${p.id}", "${p.descripcioCurta}");
			</c:forEach>
			centerMap(-30,-5,65,75); //minLng,minLat,maxLng,maxLat
		}
		
		//Per a tancar el login
		$('a#close-login-modal').click (function() {
            $('#login-modal').foundation ('reveal','close');
        })
	</script>
</body>
</html>






