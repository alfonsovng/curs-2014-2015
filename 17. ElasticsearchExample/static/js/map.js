// el mapa
var map;

// llista de funcions que s'encarregaran de treure el resaltat dels productes
var normalizeFunctions = [];

//defineix el mapa amb el seu centre i zoom
function loadMap() {
	map = new ol.Map({
		target : 'map',
		view : new ol.View({
			center : ol.proj.transform([ 1.73, 41.23 ], 'EPSG:4326', 'EPSG:3857'), // long & lat
			zoom : 15
		})
	});
	
	// capa inicial amb el mapa
	var baseLayer = new ol.layer.Tile({
		source : new ol.source.OSM()
	});
	
	// li poso opacitat per a que no resalti molt
	baseLayer.setOpacity(0.4);
	
	// afegeixo la capa al mapa
	map.addLayer(baseLayer);
	
	// si fas click al mapa, no a una icona, es deixa de resaltat els productes
	map.on('singleclick', function(evt) {
		for (var i = 0; i < normalizeFunctions.length; i++) {
			normalizeFunctions[i]();
		}
	});
	
	//geolocalitzo l'usuari
	var geolocation = new ol.Geolocation({
	  projection: map.getView().getProjection(),
	  tracking: true
	});

	//poso una icona
	var overlay = new ol.Overlay({
		element : $("<img src='img/dot.gif' title='Your position'/>")
	});
	// afegeixo la capa al mapa
	map.addOverlay(overlay);
	
	// listen to changes in position
	geolocation.on('change', function(evt) {
		overlay.setPosition(geolocation.getPosition());
	});

}

// funció que s'ha de cridar per a definir una icona
function setIconOnMap(lon, lat, elementId, buttonId, elementDesc) {
	// defineixo les icones normal (per defecte) i highligh (quan hi faci click)
	var normalIcon = $("<a href='" + elementId + "'><img src='img/icon_normal.png' title='" + elementDesc + "'/></a>");
	var highlightIcon = $("<a href='" + elementId + "'><img src='img/icon_highlight.png' title='" + elementDesc + "'/></a>");

	// defineixo la capa amb l'icona normal
	var overlay = new ol.Overlay({
		element : normalIcon
	});
	// afegeixo la capa al mapa
	map.addOverlay(overlay);

	// posiciono la capa
	var coords = ol.proj.transform([ lon, lat ], 'EPSG:4326','EPSG:3857');
	overlay.setPosition(coords);

	// afegeixo a la llista normalizeFunctions la funció que "normalitza aquest
	// producte"
	normalizeFunctions.push(function() {
		overlay.setElement(normalIcon);
		$(elementId).css("background-color", "transparent");
	});

	// quan fas click, resalta aquesta icona però abans treu el resaltat dels
	// altres. Sols pot haver-hi un resaltat!
	var clickFuncion = function() {
		for (var i = 0; i < normalizeFunctions.length; i++) {
			normalizeFunctions[i]();
		}
		overlay.setElement(highlightIcon)
		$(elementId).css("background-color", "#C844AF");
		
		//fa una animació per desplaçar-se fins el nou centre
		var pan = ol.animation.pan({
			duration: 800,
			source: (map.getView().getCenter())
		});
		map.beforeRender(pan);
		map.getView().setCenter(coords);
	};
	normalIcon.click(clickFuncion);
	
	//aquest funcionament també quan facis click al butó de cada pis
	$(buttonId).click(clickFuncion);
}

function centerMap(minLng,minLat,maxLng,maxLat) {
	var extentSquare = ol.proj.transformExtent([minLng,minLat,maxLng,maxLat], 'EPSG:4326','EPSG:3857');
	
	var myExtentButton = new ol.control.ZoomToExtent({
	    extent:extentSquare
	});
	map.addControl(myExtentButton);
	
	map.getView().fitExtent(extentSquare, map.getSize());
}