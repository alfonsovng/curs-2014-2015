package cat.jmir.collsamples.map;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;
import java.util.Set;
 
/**
 * Exemple bàsic d'us de la classe HashMap.
 *
 */
public class ExemplePasswords {
 
    public static void main(String[] args) {
         
        HashMap<String, String> passwords = new HashMap<String, String>();
        passwords.put("alfonso", "lala");
        passwords.put("pedro", "lolo");
        passwords.put("abelardo", "jaja");
        passwords.put("tomás", "lala");
        passwords.put("silvia", "lalalslñweflkñjasdfñlkjsdfñlkjdfdf");
        passwords.put("antonio", "123");
        passwords.put("alicia", "alicia");
        passwords.put("pepe", "pepe123");
         
        System.out.println();
        System.out.println("-------------------------------------------------");
        System.out.println("pepe -> " + passwords.get("pepe"));
        System.out.println("alfonso? -> " + passwords.containsKey("alfonso"));
        System.out.println("federico? -> " + passwords.containsKey("federico"));
         
        System.out.println();
        System.out.println("Canvio el password de pepe-----------------------");
        passwords.put("pepe", "chorizo");
        System.out.println("pepe -> " + passwords.get("pepe"));
        System.out.println("alfonso? -> " + passwords.containsKey("alfonso"));
        System.out.println("federico? -> " + passwords.containsKey("federico"));
         
        System.out.println();
        System.out.println("Trec alfonso del map----------------------------");
        passwords.remove("alfonso");
        System.out.println("pepe -> " + passwords.get("pepe"));
        System.out.println("alfonso? -> " + passwords.containsKey("alfonso"));
        System.out.println("federico? -> " + passwords.containsKey("federico"));
         
        System.out.println();
        System.out.println("Passwords dels usuaris que començan per 'a'-----");
        Set<String> usuarios = passwords.keySet();
        for(Iterator<String> it=usuarios.iterator();it.hasNext();) {
            String key = it.next();
            if(key.startsWith("a")) {
                String value = passwords.get(key); //l'he trobat!!!!
                System.out.println(key + " -> " + value);
            }
        }
         
        System.out.println();
        System.out.println("Tots els usuaris i passwors---------------------");
        Set<Entry<String,String>> parejas = passwords.entrySet();
        Iterator<Entry<String,String>> iterator = parejas.iterator();
        while(iterator.hasNext()) {
             
            Entry<String,String> pareja = iterator.next();
             
            String usuari = pareja.getKey();
            String password = pareja.getValue();
             
            System.out.println("[" + usuari + "]>>>>>" + password);     
        }
    }
}