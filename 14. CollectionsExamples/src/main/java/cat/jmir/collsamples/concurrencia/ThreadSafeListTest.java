package cat.jmir.collsamples.concurrencia;

import java.util.List;
 
/**
 * Exemple d'accés concurrent a llistes. Amb ArrayList es pot provocar
 * un error d'accés concurrent. En canvi, amb un Vector no es pot produir
 * per ser Thread safe.
 * 
 */
public class ThreadSafeListTest {
     
    //http://ezeon.in/blog/2014/02/thread-concurrency-test-on-arraylist-and-vector-object/
    public static void main(String[] args) {
 
        int delay=1;//thread delay
        int n=50;//item count
        List<String> list = new java.util.ArrayList<String>();//Async;not-thread-safe
        //List<String> list = new java.util.Vector<String>();//Thread-safe
         
        new Thread(new AddTask(list, n, delay)).start();
        new Thread(new RemoveTask(list, n, delay)).start();
    }
 
}