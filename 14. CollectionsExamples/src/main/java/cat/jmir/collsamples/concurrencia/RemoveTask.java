package cat.jmir.collsamples.concurrencia;

import java.util.List;

class RemoveTask implements Runnable {
 
    private int delay;
    private List<String> list;
    private int n;
     
    RemoveTask(List<String> list, int n, int delay) {
        this.list = list;
        this.n = n;
        this.delay = delay;
    }
     
    private void removeFromList(int i) {
        list.remove("element_" + i);
        System.out.println("S'ha tret l'element " + i + ": " + list);
    }
     
    public void run() {
        for(int i=0;i<n;++i) {
            removeFromList(i);
            try {
                Thread.sleep(delay);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}